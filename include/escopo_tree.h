/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>

typedef struct TIPO_ARVORE_ESCOPO escopo_tree_t;
typedef struct TIPO_ESCOPOS_LOCAIS escopo_local;


struct TIPO_ARVORE_ESCOPO{
    comp_dict_t* escopo_global;
    escopo_local* escopos_locais;
};

struct TIPO_ESCOPOS_LOCAIS{
   comp_dict_t* escopo_local;
   escopo_local* prox;
};

escopo_tree_t* escopo_criaEscopoGlobal();
escopo_tree_t* escopo_criaEscopoLocal(escopo_tree_t* escopo_prog);
escopo_tree_t* escopo_insereEscopoLocal(escopo_tree_t* escopo_prog, comp_dict_t* novo_local);
escopo_tree_t* escopo_insereEscopoGlobal(escopo_tree_t* escopo_prog, comp_dict_t* novo_global);
void escopo_imprime(escopo_tree_t* escopo_prog);
comp_dict_t* escopo_localizarEm_LocalAtual_Global(escopo_tree_t* escopo_prog, char* Chave);
comp_dict_t* escopo_defineVetor(int fator, comp_dict_t* entrada);
escopo_tree_t* escopo_insereFuncaoEscopoGlobal(escopo_tree_t* escopo_prog, comp_dict_t* novo_global, int tipo_retorno);
escopo_tree_t* escopo_insereParametrosFuncao(escopo_tree_t* escopo_prog, int tipo_param);
int escopo_retornoFuncao(escopo_tree_t* escopo_prog, char* funcao);
int confere_passagem_funcao(comp_dict_t* Dicionario, char* Chave, comp_tree_t* f_cham);
