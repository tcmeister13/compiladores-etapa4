/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include "define.h"
/*
Especifica��o:
    - Cria��o
    - Remo��o
    - Altera��o
    - Inser��o
    - N-�ria
*/

typedef struct arvore comp_tree_t;
typedef struct filho TipoFilhos;


struct arvore{
    int nodo;    /* caracter�sticas de um nodo da lista */
    int id;     /* identifica��o do pai */
    int tipo;
    int tamanho;
    int coercao; /* 0 se nao precisa, 1 se precisa*/
    comp_dict_t *entrada;
    TipoFilhos  *filhos;  /* encadeia para proximo elemento da lista */
};

struct filho{
    comp_tree_t  *arv;     /* ponteiro para o nodo da arvore */
    TipoFilhos  *prox;    /* encadeia para proximo elemento da lista */
};

/* Manipula��o da arvore */
comp_tree_t* arvore_inicializa();
comp_tree_t* arvore_remove_por_id(comp_tree_t* arv, int identificador); /* Remove nodo da �rvore com referido id */
void         arvore_imprime_profundidade_a_esquerda(comp_tree_t* arv);
void         arvore_imprime_visual(comp_tree_t* arv);
comp_tree_t* arvore_cria_nodo(int id, comp_dict_t* entrada, TipoFilhos* filhos, int id_nodo, int tipo, int tamanho);
comp_tree_t* arvore_insere_filho(comp_tree_t* arv, comp_tree_t* filho);
comp_tree_t* arvore_cria_nodo_inutil(comp_dict_t* entrada, int id_nodo, int tipo, int tamanho);
comp_tree_t* arvore_cria_nodo_pai(int id, comp_dict_t* entrada, int id_nodo, int tipo, int tamanho, comp_tree_t* filho1, comp_tree_t* filho2, comp_tree_t* filho3, comp_tree_t* filho4);
int          arvore_inferencia_de_tipo(TipoFilhos *filhos);
int          arvore_conta_filhos(TipoFilhos *filhos);
comp_tree_t* arvore_coercao(comp_tree_t *convertido, int t_conversor);
int arvore_verifica_coercao(int t_convertido, int t_conversor);
