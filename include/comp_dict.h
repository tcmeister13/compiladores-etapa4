/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#define LOCAL 0
#define GLOBAL 1
#define TIPO_ID_VARIAVEL 0
#define TIPO_ID_VETOR 2
#define TIPO_ID_FUNCAO 3
typedef struct TIPO_PARAM tipo_param;
typedef struct TIPO_DICIONARIO comp_dict_t;
typedef struct TIPO_FUNCAO tipo_funcao;


//tipo dicionario: lista encadeada para entradas
struct TIPO_DICIONARIO
{
    char *chave;
    int dado;
    int tipo;
    int tamanho;
    int tipo_id;
    tipo_funcao* funcao;
    comp_dict_t * prox;
};

struct TIPO_FUNCAO
{
   int tipo_retorno;
   tipo_param* param;
   int n_param;
};

struct TIPO_PARAM
{
   int tipo;
   tipo_param* prox;
};
   
//métodos de manipulação de entradas
void dicionario_imprime(comp_dict_t* dicio);
int dicionario_chave_nao_existente(char* Chave, comp_dict_t* Dicionario);

//métodos de manipulação do dicionário
comp_dict_t* dicionario_Criar ();
comp_dict_t* dicionario_AdicionarEntrada (char* Chave, int dado, comp_dict_t* Dicionario);
comp_dict_t* dicionario_RemoverEntrada (char* Chave, comp_dict_t* Dicionario);
comp_dict_t* dicionario_AlterarEntrada (char* Chave, int dado, comp_dict_t* Dicionario);
comp_dict_t* dicionario_AdicionaTipo (char* Chave, int tipo, int tamanho, comp_dict_t* Dicionario);
comp_dict_t* dicionario_LocalizarEntrada (char* Chave, comp_dict_t* Dicionario);
comp_dict_t* dicionario_Criar_Entrada (comp_dict_t* dicionario);
comp_dict_t* dicionario_Copiar_Entrada(comp_dict_t* dicionario);

