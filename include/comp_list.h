/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
/*
Especifica��o:
    - Cria��o
    - Remo��o
    - Concaten��o
    - Inser��o
*/

typedef struct lista comp_list_t;
typedef struct info_l TipoInfo_l;

struct lista{
    int nodo;    /* caracter�sticas de um nodo da lista */
    comp_list_t   *prox;    /* encadeia para proximo elemento da lista */
};

struct info_l{
    /* Os atributos do nodo ser�o criados ao decorrer do projeto */
    int id;     /* atributo de busca de item na lista tempor�rio */
};


comp_list_t* lista_inicializa();
TipoInfo_l* lista_cria_info(int identificador);
comp_list_t* lista_remove_primeiro(comp_list_t* l); /* remove primeiro elemento da lista */
comp_list_t* lista_remove_por_id(comp_list_t* l, int identificador); /* Percorre lista buscando o elemento pelo id - quando encontra, remove elemento da lista */
comp_list_t* lista_concatena(comp_list_t* l1, comp_list_t* l2); /* concatena duas listas, lista 2 ao fim da lista 1 */
comp_list_t* lista_insere_fim(comp_list_t* l, int novo); /* insere um novo elemento no fim da lista */
void lista_imprime(comp_list_t* l);
