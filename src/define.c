/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include "define.h"
#include <string.h>

/**
 * Retorna string compatível para o define de um tipo AST
 * name: IKS_AST_para_texto
 * @param iks: tipo AST
 * @return texto correspondente ao tipo AST
 *
 */
char* IKS_AST_para_texto(int iks)
{
	switch(iks)
	{
		case IKS_AST_PROGRAMA : return "IKS_AST_PROGRAMA";            
		case IKS_AST_FUNCAO  : return "IKS_AST_FUNCAO";    
		case IKS_AST_IF_ELSE  : return "IKS_AST_IF_ELSE";                
		case IKS_AST_DO_WHILE  : return "IKS_AST_DO_WHILE";                
		case IKS_AST_WHILE_DO : return "IKS_AST_WHILE_DO";                 
		case IKS_AST_INPUT : return "IKS_AST_INPUT";                    
		case IKS_AST_OUTPUT : return "IKS_AST_OUTPUT";                   
		case IKS_AST_ATRIBUICAO  : return "IKS_AST_ATRIBUICAO";              
		case IKS_AST_RETURN    : return "IKS_AST_RETURN";                
		case IKS_AST_BLOCO   : return "IKS_AST_BLOCO";                  
		case IKS_AST_IDENTIFICADOR  : return "IKS_AST_IDENTIFICADOR";         
		case IKS_AST_LITERAL     : return "IKS_AST_LITERAL";           
		case IKS_AST_ARIM_SOMA    : return "IKS_AST_ARIM_SOMA";            
		case IKS_AST_ARIM_SUBTRACAO  : return "IKS_AST_ARIM_SUBTRACAO";         
		case IKS_AST_ARIM_MULTIPLICACAO : return "IKS_AST_ARIM_MULTIPLICACAO";      
		case IKS_AST_ARIM_DIVISAO     : return "IKS_AST_ARIM_DIVISAO";        
		case IKS_AST_ARIM_INVERSAO    : return "IKS_AST_ARIM_INVERSAO";         // - (operador unário -)
		case IKS_AST_LOGICO_E       : return "IKS_AST_LOGICO_E";           // &&
		case IKS_AST_LOGICO_OU     : return "IKS_AST_LOGICO_OU";           // ||
		case IKS_AST_LOGICO_COMP_DIF   : return "IKS_AST_LOGICO_COMP_DIF";       // !=
		case IKS_AST_LOGICO_COMP_IGUAL  : return "IKS_AST_LOGICO_COMP_IGUAL";       // ==
		case IKS_AST_LOGICO_COMP_LE    : return "IKS_AST_LOGICO_COMP_LE";       // <=
		case IKS_AST_LOGICO_COMP_GE   : return "IKS_AST_LOGICO_COMP_GE";         // >=
		case IKS_AST_LOGICO_COMP_L      : return "IKS_AST_LOGICO_COMP_L";       // <
		case IKS_AST_LOGICO_COMP_G     : return "IKS_AST_LOGICO_COMP_G";        // >
		case IKS_AST_LOGICO_COMP_NEGACAO  : return "IKS_AST_LOGICO_COMP_NEGACAO";     // !
		case IKS_AST_VETOR_INDEXADO     : return "IKS_AST_VETOR_INDEXADO";       // para var[exp] quando o índice exp é acessado no vetor var
		case IKS_AST_CHAMADA_DE_FUNCAO   : return "IKS_AST_CHAMADA_DE_FUNCAO";     
		default: "ERROR IKS_AST";
	}

}

/**
 * Retorna string compatível para o define de um tipo de um no
 * name: IKS_TIPO_para_texto
 * @param iks: tipo de um no
 * @return texto correspondente ao tipo do no
 *
 */
char* IKS_TIPO_para_texto(int iks)
{
	switch(iks)
	{
		case IKS_INT: return "IKS_INT";
		case IKS_FLOAT: return "IKS_FLOAT";
		case IKS_CHAR: return "IKS_CHAR";
		case IKS_STRING: return "IKS_STRING";
		case IKS_BOOL: return "IKS_BOOL";
		case IKS_TIPO_ESPECIAL: return "IKS_TIPO_ESPECIAL";
		default: return "IKS_TIPO error";
		
	}
}


/**
 * Mostra na tela erro correspondente ao tipo conforme linha
 * name: Imprime_Erro
 * @param iks: tipo de um no
 * @return texto correspondente ao tipo do no
 *
 */
void Imprime_Erro (int linha, int erro)
{
	switch (erro){
	
	case IKS_ERROR_UNDECLARED: printf("Identificador não declarado na linha %d \n", linha);
			break;
	case IKS_ERROR_DECLARED: printf("Identificador já declarado na linha %d \n", linha);
			break;
	case IKS_ERROR_VARIABLE: printf("Identificador deve ser usado como variável na linha %d \n", linha);
			break;
	case IKS_ERROR_VECTOR: printf("Identificador deve ser usado como vetor na linha %d \n", linha);
			break;
	case IKS_ERROR_FUNCTION: printf("Identificador deve ser usado como função na linha %d \n", linha);
			break;
	case IKS_ERROR_WRONG_TYPE: printf("Tipos incompativeis na linha %d \n", linha);
			break;
	case IKS_ERROR_STRING_TO_X: printf("Coercao impossivel de string na linha %d \n", linha);
			break;
	case IKS_ERROR_CHAR_TO_X: printf("Coercao impossivel de caracter na linha %d \n", linha);
			break;
	case IKS_ERROR_MISSING_ARGS: printf("Faltam argumentos na linha %d \n", linha);
			break;
	case IKS_ERROR_EXCESS_ARGS: printf("Sobram argumentos na linha %d \n", linha);
			break;
	case IKS_ERROR_WRONG_TYPE_ARGS: printf("Argumentos icompatíveis na linha %d \n", linha);
			break;
	case IKS_ERROR_WRONG_PAR_INPUT: printf("Parametro nao eh identificador na linha %d \n", linha);
			break;
	case IKS_ERROR_WRONG_PAR_OUTPUT: printf("Parametro nao eh literal string ou expressao na linha %d \n", linha);
			break;
	case IKS_ERROR_WRONG_PAR_RETURN: printf("Parametro nao eh compativel com expressao na linha de retorno na linha %d \n", linha);
			break;
	case IKS_ERROR_COND_NOT_BOOL: printf("Expressao condicional nao tem tipo bool na linha %d \n", linha); 
			break;

		}
	}
	
	
/**
 * Retorna qual tipo de erro de compatibilidade de tipos ocorreu com base no que era para ser convertido
 * name: tipo_coercao_incompativel
 * @param t_convertido: tipo do no que nao pode ser convertido
 * @return erro da coerção
 *
 */
int tipo_coercao_incompativel(int t_convertido)
{
	switch(t_convertido)
	{
		case IKS_CHAR: return IKS_ERROR_CHAR_TO_X;
		case IKS_STRING : return IKS_ERROR_STRING_TO_X;
		default: return IKS_ERROR_WRONG_TYPE;
	}
}
