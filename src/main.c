/*
   main.c

   Arquivo principal do analisador sintático.
*/
#include "main.h"
extern int linha;
comp_tree_t* arvore;
escopo_tree_t* tabela_simbolos_escopo;

void yyerror (char const *mensagem)
{
  fprintf (stderr, "%s na linha %d\n", mensagem, linha);
}

int main (int argc, char **argv)
{
  gv_init("saida.dot");
  int resultado = yyparse();
  
  gv_close();
  //arvore_imprime_visual(arvore);
  //printf("retorno:%d\n",resultado);
  //if(resultado==0) escopo_imprime(tabela_simbolos_escopo);
  return resultado;
}

