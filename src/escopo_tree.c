/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include <stdlib.h>
#include "comp_dict.h"
#include "comp_tree.h"
#include "escopo_tree.h"
typedef struct TIPO_PARAM tipo_paramE;

/**
 * Cria escopo global
 * name: escopo_criaEscopoGlobal
 * @param 
 * @return escopo_tree_t alocado
 *
 */
escopo_tree_t* escopo_criaEscopoGlobal()
{
	escopo_tree_t* globale = (escopo_tree_t*) malloc(sizeof(escopo_tree_t));
	return globale;	
}

/**
 * Cria um novo escopo local na arvore de escopos (torna como primeiro filho para posteriores inserções)
 * name: escopo_criaEscopoLocal
 * @param escopo_prog: onde deve ser criado escopo local
 * @return escopo atualizado
 *
 */
escopo_tree_t* escopo_criaEscopoLocal(escopo_tree_t* escopo_prog)
{
	escopo_local* novo_local = (escopo_local*) malloc(sizeof(escopo_local));
	novo_local->prox=escopo_prog->escopos_locais;
	escopo_prog->escopos_locais=novo_local;
	
	return escopo_prog;	
}

/**
 * Insere no primeiro filho de escopos locais um novo simbolo
 * name: escopo_insereEscopoLocal
 * @param escopo_prog: onde deve ser inserido
 * @param novo_local: nova entrada de dicionario para escopo local
 * @return escopo atualizado
 *
 */
escopo_tree_t* escopo_insereEscopoLocal(escopo_tree_t* escopo_prog, comp_dict_t* novo_local)
{
	if(escopo_prog->escopos_locais->escopo_local == NULL || dicionario_LocalizarEntrada(novo_local->chave,escopo_prog->escopos_locais->escopo_local)==NULL)    
	{
		novo_local->prox = escopo_prog->escopos_locais->escopo_local;
		escopo_prog->escopos_locais->escopo_local=novo_local;
		return escopo_prog;
		
	}
	else return NULL;
	//dicionario_imprime(novo_global);
	
}

/**
 * Insere novo escopo global
 * name: escopo_insereEscopoGlobal
 * @param escopo_prog: onde deve ser inserido
 * @param novo_global: nova entrada de dicionario para escopo global
 * @return escopo atualizado
 *
 */
escopo_tree_t* escopo_insereEscopoGlobal(escopo_tree_t* escopo_prog, comp_dict_t* novo_global)
{
	if(escopo_prog->escopo_global==NULL || dicionario_LocalizarEntrada(novo_global->chave,escopo_prog->escopo_global)==NULL)
	{
		novo_global->prox=escopo_prog->escopo_global;
		escopo_prog->escopo_global=novo_global;	
	
		return escopo_prog;
	}
	else
		{ return NULL;}
}


/**
 * Verifica se há uma entrada no escopo (no local atual ou no global)
 * name: escopo_localizarEm_LocalAtual_Global
 * @param escopo_prog: onde deve ser feita a busca
 * @param Chave: chave da entrada a ser buscada
 * @return entrada correspondente à busca, sendo NULL caso não encontre nenhuma
 *
 */
comp_dict_t* escopo_localizarEm_LocalAtual_Global(escopo_tree_t* escopo_prog, char* Chave)
{
	escopo_tree_t* aux = escopo_prog;
	comp_dict_t* auxd = dicionario_Copiar_Entrada(aux->escopos_locais->escopo_local);
	comp_dict_t* entrada = dicionario_LocalizarEntrada(Chave, auxd);
	
	if(entrada==NULL)
	{
		comp_dict_t* auxg = dicionario_Copiar_Entrada(aux->escopo_global);
		comp_dict_t* entrada2 = dicionario_Criar_Entrada(dicionario_LocalizarEntrada(Chave,auxg));
		return entrada2;
	}
	else return entrada;
}

/**
 * Atualiza dicionario para tipo vetor e altera o tamanho correspondente
 * name: escopo_defineVetor
 * @param fator: valor a ser multiplicado pelo tamanho atual da entrada
 * @param entrada: entrada que será atualizada como vetor
 * @return entrada atualizada
 *
 */
comp_dict_t* escopo_defineVetor(int fator, comp_dict_t* entrada)
{
		entrada->tipo_id = TIPO_ID_VETOR;
		entrada->tamanho = entrada->tamanho*fator;
		
     	return entrada;
}

/**
 * Insere no escopo global uma funcao, atualizando seu tipo e alocando estrutura funcao para salvar parametros
 * name: escopo_insereFuncaoEscopoGlobal
 * @param escopo_prog: onde deve ser inserida a funcao
 * @param novo_global: entrada da funcao a ser inserida
 * @param tipo_retorno: tipo de retorno da funcao a ser inserida
 * @return escopo atualizado
 *
 */
escopo_tree_t* escopo_insereFuncaoEscopoGlobal(escopo_tree_t* escopo_prog, comp_dict_t* novo_global, int tipo_retorno)
{
	novo_global->tipo_id = TIPO_ID_FUNCAO;
	novo_global->funcao = (tipo_funcao*) malloc(sizeof(tipo_funcao*));
	novo_global->funcao->tipo_retorno = tipo_retorno;
	novo_global->prox = NULL;
	
	
	if(escopo_prog->escopo_global==NULL || dicionario_LocalizarEntrada(novo_global->chave,escopo_prog->escopo_global)==NULL)
	{
		
		novo_global->prox=escopo_prog->escopo_global;
		escopo_prog->escopo_global=novo_global;	
		
		return escopo_prog;
	}
	else
		{ return NULL;}
}

/**
 * Insere na estrutura funcao um novo tipo de param da funcao atual (primeiro no escopo global)
 * name: escopo_insereParametrosFuncao
 * @param escopo_prog: onde deve ser inserido o novo tipo de parametro
 * @param tipo_param: tipo do parametro da funcao
 * @return escopo atualizado
 *
 */
escopo_tree_t* escopo_insereParametrosFuncao(escopo_tree_t* escopo_prog, int tipo_param)
{
	tipo_paramE* novo = (tipo_paramE*) malloc(sizeof(tipo_paramE));
	novo->tipo = tipo_param;
	novo->prox = NULL;
	if(escopo_prog->escopo_global->funcao->param!=NULL)
		escopo_prog->escopo_global->funcao->param->prox = novo;
	else
		escopo_prog->escopo_global->funcao->param = novo;
		
	escopo_prog->escopo_global->funcao->n_param++;
	
	return escopo_prog;
}

/**
 * Busca tipo de retorno de uma funcao no escopo
 * name: escopo_retornoFuncao
 * @param escopo_prog: onde deve ser feita a busca
 * @param funcao: chave que identifica a funcao
 * @return tipo da funcao
 *
 */
int escopo_retornoFuncao(escopo_tree_t* escopo_prog, char* funcao)
{
	escopo_tree_t* aux = escopo_prog;
	comp_dict_t* auxd = dicionario_Copiar_Entrada(aux->escopos_locais->escopo_local);
	comp_dict_t* entrada = dicionario_LocalizarEntrada(funcao, auxd);
	
	if(entrada==NULL)
	{
		comp_dict_t* auxg = dicionario_Copiar_Entrada(aux->escopo_global);
		comp_dict_t* entrada2 = dicionario_Criar_Entrada(dicionario_LocalizarEntrada(funcao,auxg));
		return entrada2->funcao->tipo_retorno;
	}
	else
		return entrada->funcao->tipo_retorno;
		
}

/**
 * Imprime na tela conteudo do escopo
 * name: escopo_imprime
 * @param escopo_prog: escopo que deverá ser impresso
 * @return 
 *
 */
void escopo_imprime(escopo_tree_t* escopo_prog)
{
	printf("### TABELA DE SIMBOLOS ### \n");
	printf("## ESCOPO GLOBAL ##\n");
	dicionario_imprime(escopo_prog->escopo_global);
	printf("## ##\n");
	escopo_local* aux = escopo_prog->escopos_locais;
	while(aux!=NULL)
	{
		printf("## ESCOPO LOCAL ##\n");
		dicionario_imprime(aux->escopo_local);
		printf("## ##\n");
		aux= aux->prox;
	}
}


/**
 * Verifica se parametros passados a uma chamada de função estão corretos, devolve tipo de erro ou 0 se estiver certo
 * name: confere_passagem_funcao
 * @param Dicionario: onde buscar funcao
 * @param Chave: funcao a ser verificada
 * @param f_cham: arvore da chamada de funcao
 * @return erro que ocorreu ou sucesso (0)
 *
 */
int confere_passagem_funcao(comp_dict_t* Dicionario, char* Chave, comp_tree_t* f_cham)
{
     comp_dict_t *ant = NULL; //ponteiro auxiliar para a posição anterior
     comp_dict_t *ptaux = Dicionario; //ponteiro auxiliar para percorrer a lista
	//printf("conferindo %s funcao\n",Chave);
     /*procura o elemento na lista*/
     while (ptaux !=NULL && (strcmp(ptaux->chave, Chave)<0))
     {
          ant = ptaux;
          ptaux = ptaux->prox;
     }
    
     /*verifica se achou*/
     if (ptaux == NULL)
       return 0; /*retorna a lista original*/
     else 
     {
		//confere parametros
		TipoFilhos* filhos_f = NULL;
		if(f_cham!=NULL) { filhos_f = (TipoFilhos*) malloc(sizeof(TipoFilhos*)); filhos_f->arv = f_cham;}
		int igual=1;
		tipo_param* param_f = ptaux->funcao->param;
		
		while(param_f!=NULL && igual == 1 && filhos_f!=NULL)
		{
			//printf("Param_f->tipo %s\nfilhos_f->arv->tipo %s\n\n", IKS_TIPO_para_texto(param_f->tipo), IKS_TIPO_para_texto(filhos_f->arv->tipo));			
			comp_tree_t* arvC = filhos_f->arv;
			int tipo_convertido = arvC->tipo;
			arvC = arvore_coercao(arvC,param_f->tipo);
			if(arvC==NULL) {igual=0;}
			//if(param_f->tipo!=filhos_f->arv->tipo)
				//igual=0;
			filhos_f=filhos_f->arv->filhos;
			param_f = param_f->prox;
		}
		
		if(filhos_f==NULL && param_f!=NULL)
			return IKS_ERROR_MISSING_ARGS;//faltam argumentos
		if(filhos_f!=NULL && param_f==NULL)
			return IKS_ERROR_EXCESS_ARGS;//sobram argumentos
		if(igual!=1)
			return IKS_ERROR_WRONG_TYPE_ARGS;//argumentos incompatíveis
		
		
		return 0;
		
			
     }
}
