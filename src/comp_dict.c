/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "comp_dict.h"

/**
 * Cria um dicionário
 * name: dicionario_Criar
 * @param
 * @return NULL
 *
 */

comp_dict_t* dicionario_Criar (){
	return NULL;
}

/**
 * Cria um dicionário de uma entrada
 * name: dicionario_Criar_Entrada
 * @param dicionario: entrada a ser copiada
 * @return entrada de um dicionario
 *
 */
comp_dict_t* dicionario_Criar_Entrada (comp_dict_t* dicionario)
{
	
	
	if(dicionario==NULL)
	{
		//printf("AAA");
		return NULL;
	}
	else
	{
		//printf("OK");
		comp_dict_t* novo;
		novo = (comp_dict_t*) malloc(sizeof(comp_dict_t));
		novo->chave = (char*) malloc(strlen(dicionario->chave));
		strcpy(novo->chave,dicionario->chave);
		novo->dado = dicionario->dado;
		novo->tipo = dicionario->tipo;
		novo->tamanho = dicionario->tamanho;
		novo->tipo_id = dicionario->tipo_id;
		novo->funcao = dicionario->funcao;
		novo->prox = NULL;
		
		
		
		return novo;
	}
	
	
}


/**
 * Cria um dicionário a partir de um entrada (copia todos os próximos)
 * name: dicionario_Copia_Entrada
 * @param dicionario: entrada a ser copiada
 * @return entrada de um dicionario
 *
 */
comp_dict_t* dicionario_Copiar_Entrada(comp_dict_t* dicionario)
{
  if(dicionario==NULL)
	return NULL;
  else
  {
	comp_dict_t* novo;
	novo = (comp_dict_t*) malloc(sizeof(comp_dict_t));
	novo->chave = (char*) malloc(strlen(dicionario->chave));
	strcpy(novo->chave,dicionario->chave);
	novo->dado = dicionario->dado;
	novo->tipo = dicionario->tipo;
	novo->tamanho = dicionario->tamanho;
	novo->tipo_id = dicionario->tipo_id;
	novo->funcao = dicionario->funcao;
	novo->prox = dicionario->prox;
	return novo;
 }
	
}



/**
 * Verifica se chave não é existente no dicionário
 * name: dicionario_chave_nao_existente
 * @param Chave: chave a ser buscada
 * @param Dicionario: onde a chave deve ser buscada
 * @return 1 se não existe; 0 se existe
 *
 */
int dicionario_chave_nao_existente(char* Chave, comp_dict_t* Dicionario)
{
	comp_dict_t* aux = Dicionario;
	int nao_achou = 1;
	while(aux!=NULL && nao_achou==1)
	{
		if(strcmp(aux->chave, Chave)==0)
		{
			nao_achou = 0;
			return nao_achou;
		}

		aux=aux->prox;
	}

	return nao_achou;
}

/**
 * Adiciona nova entrada no dicionário
 * name: dicionario_AdicionarEntrada
 * @param Chave: chave da nova entrada
 * @param dado: informação da entrada
 * @param Dicionario: diconário no qual deve ser adicionado a entrada
 * @return Dicionario atualizado
 *
 */
comp_dict_t* dicionario_AdicionarEntrada (char* Chave, int dado, comp_dict_t* Dicionario){

	   comp_dict_t *novo;
       	comp_dict_t *ant = NULL; //ponteiro auxiliar para a posição anterior
       comp_dict_t *ptaux = Dicionario; //ponteiro auxiliar para percorrer a lista

	   /*aloca um novo nodo */
	   novo = (comp_dict_t*) malloc(sizeof(comp_dict_t));
	   /*insere a entrada no novo nodo*/
	   novo->chave = (char*) malloc(strlen(Chave));
	   strcpy(novo->chave,Chave);
	   strcat(novo->chave,"\0");
	   novo->dado = dado;
	   novo->tipo = 0;
	    //printf("Adicionado: #%s# de #%s# com %d\n",novo->chave,Chave,strlen(novo->chave));
	
	   
	   /*procurando a posição de inserção*/
	   while ((ptaux!=NULL) && (strcmp(ptaux->chave, novo->chave)<0) && (strcmp(ptaux->chave, novo->chave)!=0)) //se info.titulo < dados.titulo então strcmp retorna um valor menor que zero
	   {
			 ant = ptaux;
			 ptaux = ptaux->prox;
	   }
	   
	   //chave já existe
	   if((ptaux!=NULL) && (strcmp(ptaux->chave,novo->chave)==0))
	   {
		   //printf("Chave ja existente trocando linha\n");
		   ptaux->dado = dado;
		   
		   free(novo);
	   }
	   else
	   {
		   /*encadeia o elemento*/
		   if (ant == NULL) /*o anterior não existe, logo o elemento será inserido na primeira posição*/
		   {
				 //  puts("inserindo primeiro");
				   novo->prox = Dicionario;
				   Dicionario = novo;
		   }
		   else /*elemento inserido no meio da lista*/
		   {
				novo->prox = ant->prox;
				ant->prox = novo;
		   }
	   }

	//dicionario_imprime(Dicionario);
       return Dicionario;
}


/**
 * Remove uma entrada do dicionário
 * name: dicionario_RemoverEntrada
 * @param Chave: chave da entrada a ser removida
 * @param Dicionario: Dicionario no qual deve ser removida a entrada
 * @return Dicionario atualizado
 *
 */
comp_dict_t* dicionario_RemoverEntrada (char* Chave, comp_dict_t* Dicionario){
     comp_dict_t *ant = NULL; //ponteiro auxiliar para a posição anterior
     comp_dict_t *ptaux = Dicionario; //ponteiro auxiliar para percorrer a lista

     /*procura o elemento na lista*/
     while (ptaux !=NULL && (strcmp(ptaux->chave, Chave)))
     {
          ant = ptaux;
          ptaux = ptaux->prox;
     }

     /*verifica se achou*/
     if (ptaux == NULL)
       return Dicionario; /*retorna a lista original*/

    if (ant == NULL) /*vai remover o primeiro elemento*/
      Dicionario = ptaux->prox;
    else /*vai remover do meio ou do final*/
      ant->prox = ptaux->prox;

    free(ptaux);

    return Dicionario;
}

/**
 * Altera a info de uma entrada do dicionário
 * name: dicionario_AlterarEntrada
 * @param Chave: chave da entrada a ser alterada
 * @param dado: informação a ser colocada
 * @param Dicionario: dicionário na qual está a entrada a ser alterada
 * @return Dicionario atualizado
 *
 */
comp_dict_t* dicionario_AlterarEntrada (char* Chave, int dado, comp_dict_t* Dicionario){
	 comp_dict_t *ant = NULL; //ponteiro auxiliar para a posição anterior
     comp_dict_t *ptaux = Dicionario; //ponteiro auxiliar para percorrer a lista

     /*procura o elemento na lista*/
     while (ptaux !=NULL && (strcmp(ptaux->chave, Chave)<0))
     {
          ant = ptaux;
          ptaux = ptaux->prox;
     }

     /*verifica se achou*/
     if (ptaux == NULL)
       return Dicionario; /*retorna a lista original*/
     else 
       ptaux->dado = dado;

     return Dicionario;
}



/**
 * Retorna a entrada do dicionário
 * name: dicionario_AlterarEntrada
 * @param Chave: chave da entrada solicitada
 * @param Dicionario: dicionário na qual está a entrada
 * @return Entrada da chave
 *
 */
comp_dict_t* dicionario_LocalizarEntrada (char* Chave, comp_dict_t* Dicionario){
	
    comp_dict_t *ptaux = Dicionario; //ponteiro auxiliar para percorrer a lista
	int achou = 1;
	//printf("P: %s\n",Chave);
	
	//dicionario_imprime(Dicionario);
     /*procura o elemento na lista*/
     while (ptaux !=NULL && achou!=0)
     {
	//printf("A: %s -",ptaux->chave);
          achou = strcmp(Chave,ptaux->chave);	
          if(achou!=0)
		ptaux = ptaux->prox;
     }
	
		//printf("achou %d\n",achou);
     /*verifica se achou*/
    if(achou!=0)
		return NULL;
	else 
		return ptaux;

     
}


/**
 * Adiciona o tipo para um elemento
 * name: dicionario_AdicionaTipo
 * @param Chave: chave da entrada a ser alterada
 * @param tipo: tipo do elemento da chave
 * @param Dicionario: dicionário na qual está a entrada a ser alterada
 * @return Dicionario atualizado
 *
 */
comp_dict_t* dicionario_AdicionaTipo (char* Chave, int tipo,int tamanho, comp_dict_t* Dicionario){
	 comp_dict_t *ant = NULL; //ponteiro auxiliar para a posição anterior
     comp_dict_t *ptaux = Dicionario; //ponteiro auxiliar para percorrer a lista
	
     /*procura o elemento na lista*/
    int achou = 1;
	
     while (ptaux !=NULL && achou!=0)
     {
          achou = strcmp(Chave,ptaux->chave);	
          if(achou!=0)
			ptaux = ptaux->prox;
     }
     
     if (achou!=0)
	{
       return NULL; /*retorna a lista original*/
	}
     else
	{
		ptaux->tipo = tipo;
		ptaux->tamanho = tamanho;

	}
       

     return Dicionario;

}

/**
 * Mostra o dicionário na tela
 * name: dicionario_imprime
 * @param dicio: Dicionário a ser impresso na tela
 * @return
 *
 */
void dicionario_imprime(comp_dict_t* dicio)
{
	comp_dict_t* aux = dicio;
	printf("IMPRIMINDO DICIONARIO\n");
	while(aux!=NULL)
	{
		printf("\nEntrada chave: %s\n", aux->chave);
		printf("Entrada informacao: %d\n", aux->dado);
    		char* tipo = (char*) IKS_TIPO_para_texto(aux->tipo);
		printf("Tipo chave: %s\n",tipo);
		printf("Tamanho chave: %d\n", aux->tamanho);
		printf("Tipo id: %d\n", aux->tipo_id);
		if(aux->tipo_id == TIPO_ID_FUNCAO)
		{
			printf("Retorno funcao: %d\n", aux->funcao->tipo_retorno);
			int i;
			tipo_param* auxtp = aux->funcao->param;
			for(i=0; i<aux->funcao->n_param;i++)
			{
				printf("Param %d tipo %d\n",i,auxtp->tipo);
				auxtp=auxtp->prox;
			}
}

		

		aux = aux->prox;
	}
	printf("\nFIM DICIONARIO\n\n");
}
