/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include <stdlib.h>
#include "iks_ast.h"
#include "define.h"
#include "comp_dict.h"
#include "comp_tree.h"

/**
 * Cria uma nova �rvore
 * name: arvore_inicializa
 * @param
 * @return NULL
 *
 */
comp_tree_t* arvore_inicializa(){
    return NULL;
}


/**
 * Remove nodo da �rvore com referido id
 * name: arvore_remove_por_id
 * @param arv: onde ser� removido nodo
 * @param identificador: qual elemento ser� removido
 * @return Arvore atualizada
 *
 */
comp_tree_t* arvore_remove_por_id(comp_tree_t* arv, int identificador){
    TipoFilhos *aux_filhos;
    TipoFilhos *anterior;
    if(arv == NULL){return NULL;} /* arvore vazia */
    if(arv->nodo == identificador){ /* estou no nodo com o id */
        free(arv);
        printf("Nodo %d encontrado e removido!\n", identificador);
        return NULL;
    }
    /* testar cada filho, se tiver o id, remove da lista de filhos */
    aux_filhos = arv->filhos;
    anterior = aux_filhos;
    if(aux_filhos == NULL){ return arv; }
    /* se for o primeiro filho */
    if(aux_filhos->arv->nodo == identificador){
        arv->filhos = arv->filhos->prox;
        printf("Nodo %d encontrado e removido!\n", identificador);
        return arv;
    }
    while(aux_filhos != NULL){
        if(aux_filhos->arv->nodo == identificador){
            anterior->prox = aux_filhos->prox;
            free(aux_filhos);
            printf("Nodo %d encontrado e removido!\n", identificador);
            return arv;
        }
        anterior = aux_filhos;
        aux_filhos = aux_filhos->prox;
    }
    /* se n�o foi nenhum dos filhos, testar para todos os filhos (recurs�o) */
    while(aux_filhos != NULL){
        aux_filhos->arv = arvore_remove_por_id(aux_filhos->arv, identificador);
        aux_filhos = aux_filhos->prox;
    }
    return arv;
}


/**
 * Imprime na tela arvore conforme profundidade a esquerda
 * name: arvore_imprime_profundidade_a_esquerda
 * @param arv: arvore a ser impressa
 * @return
 *
 */
void arvore_imprime_profundidade_a_esquerda(comp_tree_t* arv){
    TipoFilhos *filhos;
    if(arv == NULL){
        printf("Arvore vazia\n");
        return;
    }
    filhos = arv->filhos;
    printf("%d\n", arv->nodo);
    if(arv->entrada!=NULL)
    {
    	printf("%s\n", arv->entrada->chave);
    }
    while(filhos != NULL){
        arvore_imprime_profundidade_a_esquerda(filhos->arv);
        filhos = filhos->prox;
    }
    return;
}

/**
 * Imprime arvore (printf) na tela
 * name: arvore_imprime_visual
 * @param arv: arvore a ser impressa
 * @return
 *
 */
void arvore_imprime_visual(comp_tree_t* arv){
    TipoFilhos *filhos;
    /* se nullo, imprime null */
    if(arv == NULL){
        printf("Arvore vazia\n");
        return;
    }
    filhos = arv->filhos;
    /* imprime dados do nodo atual */
    printf("Nodo id: %d\n", arv->id);
    char* nodo = (char*) IKS_AST_para_texto(arv->nodo);
    char* tipo = (char*) IKS_TIPO_para_texto(arv->tipo);
    printf("Nodo info: %s\n", nodo);
    printf("Nodo tipo: %s\n", tipo);
    printf("Nodo tamanho: %d\n", arv->tamanho);
	printf("Nodo com coercao: %d\n", arv->coercao);
	if(arv->entrada!=NULL)
	{
		printf("Entrada dicionario: %s\n", arv->entrada->chave);
	}
	else
	{
		printf("Sem entrada dicionario\n");
	}

	printf("Filhos:\n");
    /* se nodo sem filhos */
    if(arv->filhos == NULL){
        printf("\tnull\n");
        return;
    }
    /* imprime id filhos */
    while(filhos != NULL){
        printf("\t%d\n", filhos->arv->id);
        filhos = filhos->prox;
    }
	printf("\n");
    /* chama recurs�o para cada filho */
    filhos = arv->filhos;
    while(filhos != NULL){
        arvore_imprime_visual(filhos->arv);
        filhos = filhos->prox;
    }

    return;
}


/**
 * Cria nodo numa arvore e j� inclui no grafo gv
 * name: arvore_cria_nodo
 * @param id: valor padr�o de identificacao do nodo
 * @param entrada: entrada do dicionario referente ao nodo
 * @param filhos: filhos do nodo a ser criado
 * @param id_nodo: id de identificacao do nodo
 * @return Nodo �rvore com filhos
 *
 */
comp_tree_t* arvore_cria_nodo(int id, comp_dict_t* entrada, TipoFilhos* filhos, int id_nodo, int tipo, int tamanho){

	comp_tree_t* arv = (comp_tree_t*) malloc(sizeof(comp_tree_t));

	arv->entrada = entrada;
	if(entrada!=NULL) { arv->entrada->tipo = tipo; arv->entrada->tamanho = tamanho;}
	arv->nodo = id;
	arv->id = id_nodo;
	arv->filhos = filhos;
	arv->tipo = tipo;
	arv->coercao = 0;
	arv->tamanho = tamanho;

	if(entrada!=NULL)
	{
		gv_declare(id, arv, entrada->chave);
	}
	else
	{

		gv_declare(id,arv,NULL);
	}

	//conecta todos os filhos ao pai rec�m criado
	TipoFilhos* aux = filhos;
	while(aux!=NULL)
	{
		gv_connect(arv,aux->arv);
		aux = aux->prox;
	}
	return arv;

}

/**
 * Cria pai para 4 filhos - redu��o no c�digo do parser
 * name: arvore_cria_nodo_pai
 * @param id: valor padr�o de identificacao do nodo
 * @param entrada: entrada do dicionario referente ao nodo
 * @param id_nodo: id de identificacao do nodo
 * @param arv: nodo filho a ser incluido ou NULL
 * @param arv: nodo filho a ser incluido ou NULL
 * @param arv: nodo filho a ser incluido ou NULL
 * @param arv: nodo filho a ser incluido ou NULL
 * @return Nodo �rvore com filhos
 *
 */
 comp_tree_t* arvore_cria_nodo_pai( int id, comp_dict_t* entrada, int id_nodo, int tipo, int tamanho,
 									  comp_tree_t* filho1, comp_tree_t* filho2, comp_tree_t* filho3, comp_tree_t* filho4){

   comp_tree_t* pai = NULL;
   TipoFilhos *filho_1 = NULL;
   /* cria lista de filhos */
   if(filho1 != NULL){
   	filho_1 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
   	filho_1->arv = filho1;
   	filho_1->prox = NULL;
   	if(filho2 != NULL){
   		TipoFilhos *filho_2 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
   		filho_2->arv = filho2;
   		filho_2->prox = NULL;
   		filho_1->prox = filho_2;
   		if(filho3 != NULL){
   			TipoFilhos *filho_3 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
   			filho_3->arv = filho3;
   			filho_3->prox = NULL;
   			filho_2->prox = filho_3;
   			if(filho4 != NULL){
   				TipoFilhos *filho_4 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
   				filho_4->arv = filho4;
   			   filho_4->prox = NULL;
   				filho_3->prox = filho_4;
   			}
   		}
   	}
   }

   /* cria nodo com filhos */
   pai = arvore_cria_nodo(id, entrada, filho_1, id_nodo, tipo, tamanho);

 	return pai;
 }


/**
 * Cria nodo numa arvore sem incluir no grafo gv
 * name: arvore_cria_nodo_inutil
 * @param entrada: entrada do dicionario referente ao nodo
 * @param id_nodo: id de identificacao do nodo
 * @return Nodo �rvore com entrada dicionario e id
 *
 */
comp_tree_t* arvore_cria_nodo_inutil(comp_dict_t* entrada,int id_nodo, int tipo, int tamanho){

	comp_tree_t* arv = (comp_tree_t*) malloc(sizeof(comp_tree_t));
	arv->entrada = entrada;
	arv->entrada->tipo = tipo;
	arv->entrada->tamanho = tamanho;

	arv->id = id_nodo;
	arv->tipo = tipo;
	arv->tamanho = tamanho;

	return arv;

}

/**
 * Insere filho na lista de filhos de um nodo j� criado
 * name: arvore_insere_filho
 * @param arv: nodo a ter filho incluido
 * @param novo_filho: novo filho a ser incluido
 * @return Nodo �rvore com mais um filho
 *
 */
comp_tree_t* arvore_insere_filho(comp_tree_t* arv, comp_tree_t* filho){

	TipoFilhos* aux;

	TipoFilhos *novo_filho = NULL;

	if(filho!=NULL){
		novo_filho= (TipoFilhos*) malloc(sizeof(TipoFilhos));
		novo_filho->arv = (comp_tree_t*)filho;
		novo_filho->prox = NULL;
	}

	if(arv->filhos!=NULL)
	{
		aux = arv->filhos;
		while(aux->prox!=NULL)
		{
			aux= aux->prox;
		}
		aux->prox = novo_filho;
		if(novo_filho!=NULL)
			gv_connect(arv,novo_filho->arv);
	}
	else
	{
		arv->filhos = novo_filho;
		if(novo_filho!=NULL)
			gv_connect(arv,novo_filho->arv);
	}

	return arv;
}

  /**
 * Retorna tipo do nodo pai da arvore
 * name: arvore_retorna_tipo
 * @param filhos: lista de filhos do nodo
 * @return Tipo do nodo pai
 *
 */
int arvore_inferencia_de_tipo(TipoFilhos *filhos){
    int tipo, num_de_filhos;
    TipoFilhos *filho1 = filhos;
 	
    if (filhos == NULL) return 0;

    num_de_filhos = arvore_conta_filhos(filhos);

    switch (num_de_filhos){
        case 1:
            {tipo = filho1->arv->tipo;}
            break;
        case 2:
            {TipoFilhos *filho2 = filhos->prox;
            if(filho2->arv->tipo == IKS_FLOAT || filho1->arv->tipo == IKS_FLOAT){ // IKS_FLOAT == 2
                tipo = IKS_FLOAT;
            }else if(filho2->arv->tipo == IKS_INT || filho1->arv->tipo == IKS_INT){ // IKS_INT == 1
                    tipo = IKS_INT;
                }else if(filho2->arv->tipo == IKS_BOOL|| filho1->arv->tipo == IKS_BOOL){ //IKS_BOOL == 5
                        tipo = IKS_BOOL;
                    }else{
                        tipo = filho2->arv->tipo;
                }}
            break;
        case 3:
            {TipoFilhos *filho2 = filhos->prox;
            TipoFilhos *filho3 = filhos->prox->prox;
            if(filho2->arv->tipo == IKS_FLOAT || filho1->arv->tipo == IKS_FLOAT || filho3->arv->tipo == IKS_FLOAT){ // IKS_FLOAT == 2
                tipo = IKS_FLOAT;
            }else if(filho2->arv->tipo == IKS_INT || filho1->arv->tipo == IKS_INT || filho3->arv->tipo == IKS_INT){ // IKS_INT == 1
                    tipo = IKS_INT;
                }else if(filho2->arv->tipo == IKS_BOOL || filho1->arv->tipo == IKS_BOOL || filho3->arv->tipo == IKS_BOOL){ //IKS_BOOL == 5
                        tipo = IKS_BOOL;
                    }else{
                        tipo = filho3->arv->tipo;
                }}
            break;
        case 4:
            {TipoFilhos *filho2 = filhos->prox;
            TipoFilhos *filho3 = filhos->prox->prox;
            TipoFilhos *filho4 = filhos->prox->prox->prox;
            if(filho2->arv->tipo == IKS_FLOAT || filho1->arv->tipo == IKS_FLOAT || filho3->arv->tipo == IKS_FLOAT || filho4->arv->tipo == IKS_FLOAT){ // IKS_FLOAT == 2
                tipo = IKS_FLOAT;
            }else if(filho2->arv->tipo == IKS_INT || filho1->arv->tipo == IKS_INT || filho3->arv->tipo == IKS_INT || filho4->arv->tipo == IKS_INT){ // IKS_INT == 1
                    tipo = IKS_INT;
                }else if(filho2->arv->tipo == IKS_BOOL || filho1->arv->tipo == IKS_BOOL || filho3->arv->tipo == IKS_BOOL || filho4->arv->tipo == IKS_BOOL){ //IKS_BOOL == 5
                        tipo = IKS_BOOL;
                    }else{
                        tipo = filho4->arv->tipo;
                }}
            break;
        default:
            printf("Numero invalido de filhos!\n");
            tipo = 0;
            break;
    }

    return tipo;
}

 /**
 * Retorna numero de filhos na lista
 * name: arvore_conta_filhos
 * @param filhos: lista de filhos do nodo
 * @return Numero de filhos
 *
 */
 int arvore_conta_filhos(TipoFilhos *filhos){
     int num_de_filhos = 0;
     TipoFilhos *ptaux = filhos;

     while(ptaux != NULL){
        num_de_filhos++;
        ptaux = ptaux->prox;
     }
     return num_de_filhos;
 }


/**
 * Retorna nodo atualizado com campo coer��o
 * name: arvore_coercao
 * @param convertido: nodo que poder� ser convertido
 * @param t_conversor: tipo do conversor
 * @return nodo convertido atualizado
 *
 */
comp_tree_t* arvore_coercao(comp_tree_t *convertido, int t_conversor)
{
	if(convertido->tipo == t_conversor)
		return convertido;
	if(arvore_verifica_coercao(convertido->tipo,t_conversor))
		convertido->coercao=1;
	else return NULL;
}

/**
 * Retorna se � possivel coer��o (1) ou n�o (0)
 * name: arvore_verifica_coercao
 * @param t_convertido: tipo do nodo que quer ser convertido
 * @param t_conversor: tipo do conversor
 * @return se � poss�vel coer��o
 *
 */
int arvore_verifica_coercao(int t_convertido, int t_conversor)
{
	if(t_convertido == IKS_INT)
	{
		if(t_conversor == IKS_FLOAT || t_conversor == IKS_BOOL)
			return 1;
		else
			return 0;
	}
	if(t_convertido == IKS_BOOL)
	{
		if(t_conversor == IKS_FLOAT || t_conversor == IKS_INT)
			return 1;
		else
			return 0;
	}
	if(t_convertido == IKS_FLOAT)
	{
		if(t_conversor == IKS_INT || t_conversor == IKS_BOOL)
			return 1;
		else
			return 0;
	}
	
	return 0;
}





