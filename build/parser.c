
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.4.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Copy the first part of user declarations.  */

/* Line 189 of yacc.c  */
#line 1 "parser.y"

/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include <string.h>
#include "main.h"

//tamanho em bytes de cada tipo (etapa4)
#define tamanho_int 4
#define tamanho_float 8
#define tamanho_char 1
#define tamanho_bool 1
//string = tamanho char*nºcarac
//vetor = tamanho*tipo

extern int linha;
extern comp_dict_t* dicionario;
extern char* yytext;
extern char* sem_primeiro;
extern int tam_string;
extern comp_tree_t* arvore;
extern escopo_tree_t* tabela_simbolos_escopo;
comp_dict_t* simbolo;
int tipo_id;
int tamanho_id;
int id_nodo=0;
int retorno;
int nodo_nao_valido = 0;
int declaracao_id = 1;
char* ultimo_id;
int valor_vetor_id;
int funcao_tipo_retorno = -1; //variavel para salvar o tipo de retorno da função em cursão (para converir return)
char * ultimo_id_funcao; //variavel para salvar id da função para adicionar parametros (cabeçalho)
comp_dict_t* ultimo_simbolo_funcao;


/* Line 189 of yacc.c  */
#line 113 "/home/aluno/compiladores-etapa4/build/parser.c"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     TK_PR_INT = 258,
     TK_PR_FLOAT = 259,
     TK_PR_BOOL = 260,
     TK_PR_CHAR = 261,
     TK_PR_STRING = 262,
     TK_PR_IF = 263,
     TK_PR_THEN = 264,
     TK_PR_ELSE = 265,
     TK_PR_WHILE = 266,
     TK_PR_DO = 267,
     TK_PR_INPUT = 268,
     TK_PR_OUTPUT = 269,
     TK_PR_RETURN = 270,
     TK_OC_LE = 271,
     TK_OC_GE = 272,
     TK_OC_EQ = 273,
     TK_OC_NE = 274,
     TK_OC_AND = 275,
     TK_OC_OR = 276,
     TK_LIT_INT = 277,
     TK_LIT_FLOAT = 278,
     TK_LIT_FALSE = 279,
     TK_LIT_TRUE = 280,
     TK_LIT_CHAR = 281,
     TK_LIT_STRING = 282,
     TK_IDENTIFICADOR = 283,
     TOKEN_ERRO = 284,
     UMINUS = 285
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 214 of yacc.c  */
#line 41 "parser.y"
	
	struct comp_tree_t* no;
	int ast_op;
	int tipo;
	struct comp_dict_t* tabela;
	int vetorTam;



/* Line 214 of yacc.c  */
#line 189 "/home/aluno/compiladores-etapa4/build/parser.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


/* Copy the second part of user declarations.  */


/* Line 264 of yacc.c  */
#line 201 "/home/aluno/compiladores-etapa4/build/parser.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  3
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   256

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  50
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  43
/* YYNRULES -- Number of rules.  */
#define YYNRULES  94
/* YYNRULES -- Number of states.  */
#define YYNSTATES  148

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   287

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    37,     2,     2,     2,     2,     2,     2,
      44,    45,    32,    30,    46,    31,     2,    33,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    40,    41,
      35,    49,    36,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    42,     2,    43,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    47,     2,    48,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    34,    38,    39
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     4,     7,    10,    11,    14,    16,    21,
      22,    29,    30,    33,    34,    35,    40,    41,    46,    47,
      55,    56,    58,    59,    66,    70,    74,    78,    82,    83,
      85,    87,    89,    91,    93,    95,    97,    99,   101,   105,
     107,   109,   113,   120,   129,   136,   143,   146,   149,   153,
     155,   157,   160,   161,   167,   168,   170,   174,   176,   180,
     184,   188,   191,   194,   196,   198,   200,   202,   204,   206,
     208,   210,   212,   214,   216,   218,   220,   222,   224,   226,
     228,   233,   235,   237,   239,   241,   243,   245,   247,   249,
     251,   253,   255,   257,   259
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      51,     0,    -1,    -1,    52,    53,    -1,    54,    53,    -1,
      -1,    60,    53,    -1,     1,    -1,    88,    40,    57,    41,
      -1,    -1,    88,    40,    91,    41,    56,    55,    -1,    -1,
      91,    58,    -1,    -1,    -1,    59,    42,    90,    43,    -1,
      -1,    62,    55,    61,    67,    -1,    -1,    88,    40,    91,
      63,    44,    64,    45,    -1,    -1,    65,    -1,    -1,    88,
      40,    91,    46,    66,    65,    -1,    88,    40,    91,    -1,
      47,    68,    48,    -1,    69,    41,    68,    -1,    70,    41,
      68,    -1,    -1,    41,    -1,    69,    -1,    70,    -1,    72,
      -1,    73,    -1,    74,    -1,    75,    -1,    78,    -1,    79,
      -1,    47,    68,    48,    -1,    69,    -1,    70,    -1,    87,
      49,    83,    -1,     8,    44,    83,    45,     9,    71,    -1,
       8,    44,    83,    45,     9,    71,    10,    71,    -1,    11,
      44,    83,    45,    12,    71,    -1,    12,    71,    11,    44,
      83,    45,    -1,    13,    91,    -1,    14,    76,    -1,    77,
      46,    76,    -1,    77,    -1,    83,    -1,    15,    83,    -1,
      -1,    91,    44,    80,    81,    45,    -1,    -1,    82,    -1,
      83,    46,    82,    -1,    83,    -1,    44,    83,    45,    -1,
      83,    84,    83,    -1,    83,    85,    83,    -1,    31,    83,
      -1,    37,    83,    -1,    86,    -1,    30,    -1,    31,    -1,
      32,    -1,    33,    -1,    21,    -1,    20,    -1,    16,    -1,
      17,    -1,    18,    -1,    19,    -1,    35,    -1,    36,    -1,
      79,    -1,    89,    -1,    87,    -1,    91,    -1,    91,    42,
      83,    43,    -1,     3,    -1,     4,    -1,     5,    -1,     6,
      -1,     7,    -1,    90,    -1,    23,    -1,    26,    -1,    92,
      -1,    25,    -1,    24,    -1,    22,    -1,    28,    -1,    27,
      -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   131,   131,   131,   139,   140,   141,   144,   148,   155,
     154,   159,   161,   165,   166,   166,   169,   169,   172,   171,
     184,   184,   186,   185,   190,   197,   199,   200,   201,   202,
     203,   204,   206,   207,   208,   209,   210,   211,   213,   216,
     217,   220,   231,   239,   247,   255,   264,   274,   281,   287,
     288,   290,   301,   301,   318,   318,   319,   326,   328,   329,
     336,   343,   349,   355,   357,   358,   359,   360,   362,   363,
     364,   365,   366,   367,   368,   369,   371,   372,   373,   375,
     383,   403,   404,   405,   406,   407,   409,   410,   416,   422,
     423,   429,   435,   448,   465
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "TK_PR_INT", "TK_PR_FLOAT", "TK_PR_BOOL",
  "TK_PR_CHAR", "TK_PR_STRING", "TK_PR_IF", "TK_PR_THEN", "TK_PR_ELSE",
  "TK_PR_WHILE", "TK_PR_DO", "TK_PR_INPUT", "TK_PR_OUTPUT", "TK_PR_RETURN",
  "TK_OC_LE", "TK_OC_GE", "TK_OC_EQ", "TK_OC_NE", "TK_OC_AND", "TK_OC_OR",
  "TK_LIT_INT", "TK_LIT_FLOAT", "TK_LIT_FALSE", "TK_LIT_TRUE",
  "TK_LIT_CHAR", "TK_LIT_STRING", "TK_IDENTIFICADOR", "TOKEN_ERRO", "'+'",
  "'-'", "'*'", "'/'", "UMINUS", "'<'", "'>'", "'!'", "\"expr\"", "\"op\"",
  "':'", "';'", "'['", "']'", "'('", "')'", "','", "'{'", "'}'", "'='",
  "$accept", "s", "$@1", "prog", "decl_global", "decl_local", "$@2",
  "acesso_id_esp", "vetor_esp", "$@3", "func", "$@4", "cab", "$@5",
  "param", "lst_param", "$@6", "corpo", "seq_com", "com_s", "com_b",
  "com_u", "atrib", "fluxo", "entrada", "saida", "lst_saida", "elem_saida",
  "retorno", "f_cham", "$@7", "passag", "lst_p", "expr", "opArit", "opLog",
  "elemento", "acesso_id", "tipo", "tipo_lit", "lit_int_pos", "tipo_id",
  "tipo_string", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
      43,    45,    42,    47,   285,    60,    62,    33,   286,   287,
      58,    59,    91,    93,    40,    41,    44,   123,   125,    61
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    50,    52,    51,    53,    53,    53,    53,    54,    56,
      55,    55,    57,    58,    59,    58,    61,    60,    63,    62,
      64,    64,    66,    65,    65,    67,    68,    68,    68,    68,
      68,    68,    69,    69,    69,    69,    69,    69,    70,    71,
      71,    72,    73,    73,    73,    73,    74,    75,    76,    76,
      77,    78,    80,    79,    81,    81,    82,    82,    83,    83,
      83,    83,    83,    83,    84,    84,    84,    84,    85,    85,
      85,    85,    85,    85,    85,    85,    86,    86,    86,    87,
      87,    88,    88,    88,    88,    88,    89,    89,    89,    89,
      89,    89,    90,    91,    92
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     0,     2,     2,     0,     2,     1,     4,     0,
       6,     0,     2,     0,     0,     4,     0,     4,     0,     7,
       0,     1,     0,     6,     3,     3,     3,     3,     0,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     3,     1,
       1,     3,     6,     8,     6,     6,     2,     2,     3,     1,
       1,     2,     0,     5,     0,     1,     3,     1,     3,     3,
       3,     2,     2,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       4,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       2,     0,     0,     1,     7,    81,    82,    83,    84,    85,
       3,     0,     0,    11,     0,     4,     6,    16,     0,     0,
       0,     0,    93,     0,    13,    28,    17,     0,     8,    12,
       0,     0,     0,     0,     0,     0,     0,     0,    29,    28,
       0,    30,    31,    32,    33,    34,    35,    36,    37,     0,
      79,     9,     0,    20,     0,     0,    39,    40,     0,    46,
      92,    87,    91,    90,    88,    94,     0,     0,     0,    47,
      49,    76,    50,    63,    78,    77,    86,    89,    51,     0,
      25,    28,    28,     0,     0,    52,    11,     0,     0,    21,
       0,     0,     0,     0,    61,    62,     0,     0,    70,    71,
      72,    73,    69,    68,    64,    65,    66,    67,    74,    75,
       0,     0,    38,    26,    27,    41,     0,    54,    10,    15,
      19,     0,     0,     0,     0,    58,    48,    59,    60,    80,
       0,    55,    57,    24,     0,     0,     0,    53,     0,    22,
      42,    44,    45,    56,     0,     0,    23,    43
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,     2,    10,    11,    17,    86,    23,    29,    30,
      12,    20,    13,    31,    88,    89,   144,    26,    40,    41,
      42,    58,    43,    44,    45,    46,    69,    70,    47,    71,
     117,   130,   131,    72,   110,   111,    73,    74,    14,    75,
      76,    50,    77
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -122
static const yytype_int16 yypact[] =
{
    -122,     5,    87,  -122,  -122,  -122,  -122,  -122,  -122,  -122,
    -122,    87,    87,    40,   -29,  -122,  -122,  -122,   -19,     2,
      -6,     2,  -122,    11,   -26,    14,  -122,    15,  -122,  -122,
      23,    30,    33,    34,    55,     2,   198,   198,  -122,    14,
      31,    39,    43,  -122,  -122,  -122,  -122,  -122,  -122,    32,
      20,  -122,    63,    40,   198,   198,  -122,  -122,    75,  -122,
    -122,  -122,  -122,  -122,  -122,  -122,   198,   198,   198,  -122,
      49,  -122,   220,  -122,  -122,  -122,  -122,  -122,   220,    48,
    -122,    14,    14,   198,   198,  -122,    40,    54,    53,  -122,
      59,   116,   139,    56,    18,  -122,   146,   198,  -122,  -122,
    -122,  -122,  -122,  -122,  -122,  -122,  -122,  -122,  -122,  -122,
     198,   198,  -122,  -122,  -122,   220,   176,   198,  -122,  -122,
    -122,     2,    92,    96,   198,  -122,  -122,  -122,  -122,  -122,
      69,  -122,   108,    70,    55,    55,   169,  -122,   198,  -122,
     109,  -122,  -122,  -122,    40,    55,  -122,  -122
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
    -122,  -122,  -122,    60,  -122,    36,  -122,  -122,  -122,  -122,
    -122,  -122,  -122,  -122,  -122,   -14,  -122,  -122,   -31,   -30,
     -28,  -121,  -122,  -122,  -122,  -122,    21,  -122,  -122,   -24,
    -122,  -122,   -18,   -35,  -122,  -122,  -122,   -22,   -13,  -122,
      90,   -12,  -122
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -19
static const yytype_int16 yytable[] =
{
      18,    48,    78,    49,    56,     3,    57,    24,    79,    27,
      48,    19,    49,   140,   141,    48,   -14,    49,   -18,    91,
      92,    21,    32,    59,   147,    33,    34,    35,    36,    37,
      22,    94,    95,    96,    98,    99,   100,   101,   102,   103,
      90,    25,    22,     5,     6,     7,     8,     9,   115,   116,
     113,   114,    28,   108,   109,    38,    51,    48,    48,    49,
      49,    39,    84,    32,    85,    52,    33,    34,    35,    36,
      37,    15,    16,    18,    53,   127,   128,    54,    55,    80,
      81,    83,   132,    22,    82,    60,    93,    -5,     4,   136,
       5,     6,     7,     8,     9,    97,   112,   119,   120,   121,
     124,   134,    39,   132,    56,    56,    57,    57,   135,   133,
      48,    48,    49,    49,   137,    56,   139,    57,   126,   145,
     143,    48,   118,    49,    98,    99,   100,   101,   102,   103,
     146,    90,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,    87,   108,   109,     0,   104,   105,   106,   107,
       0,   108,   109,     0,   138,    98,    99,   100,   101,   102,
     103,   122,    98,    99,   100,   101,   102,   103,     0,   104,
     105,   106,   107,     0,   108,   109,   104,   105,   106,   107,
       0,   108,   109,     0,   123,    98,    99,   100,   101,   102,
     103,   125,    98,    99,   100,   101,   102,   103,     0,   104,
     105,   106,   107,     0,   108,   109,   104,   105,   106,   107,
       0,   108,   109,     0,   142,     0,     0,     0,     0,   129,
      60,    61,    62,    63,    64,    65,    22,     0,     0,    66,
       0,     0,     0,     0,     0,    67,    98,    99,   100,   101,
     102,   103,    68,     0,     0,     0,     0,     0,     0,     0,
     104,   105,   106,   107,     0,   108,   109
};

static const yytype_int16 yycheck[] =
{
      13,    25,    37,    25,    34,     0,    34,    19,    39,    21,
      34,    40,    34,   134,   135,    39,    42,    39,    44,    54,
      55,    40,     8,    35,   145,    11,    12,    13,    14,    15,
      28,    66,    67,    68,    16,    17,    18,    19,    20,    21,
      53,    47,    28,     3,     4,     5,     6,     7,    83,    84,
      81,    82,    41,    35,    36,    41,    41,    81,    82,    81,
      82,    47,    42,     8,    44,    42,    11,    12,    13,    14,
      15,    11,    12,    86,    44,   110,   111,    44,    44,    48,
      41,    49,   117,    28,    41,    22,    11,     0,     1,   124,
       3,     4,     5,     6,     7,    46,    48,    43,    45,    40,
      44,     9,    47,   138,   134,   135,   134,   135,    12,   121,
     134,   135,   134,   135,    45,   145,    46,   145,    97,    10,
     138,   145,    86,   145,    16,    17,    18,    19,    20,    21,
     144,   144,    16,    17,    18,    19,    20,    21,    30,    31,
      32,    33,    52,    35,    36,    -1,    30,    31,    32,    33,
      -1,    35,    36,    -1,    46,    16,    17,    18,    19,    20,
      21,    45,    16,    17,    18,    19,    20,    21,    -1,    30,
      31,    32,    33,    -1,    35,    36,    30,    31,    32,    33,
      -1,    35,    36,    -1,    45,    16,    17,    18,    19,    20,
      21,    45,    16,    17,    18,    19,    20,    21,    -1,    30,
      31,    32,    33,    -1,    35,    36,    30,    31,    32,    33,
      -1,    35,    36,    -1,    45,    -1,    -1,    -1,    -1,    43,
      22,    23,    24,    25,    26,    27,    28,    -1,    -1,    31,
      -1,    -1,    -1,    -1,    -1,    37,    16,    17,    18,    19,
      20,    21,    44,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      30,    31,    32,    33,    -1,    35,    36
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    51,    52,     0,     1,     3,     4,     5,     6,     7,
      53,    54,    60,    62,    88,    53,    53,    55,    88,    40,
      61,    40,    28,    57,    91,    47,    67,    91,    41,    58,
      59,    63,     8,    11,    12,    13,    14,    15,    41,    47,
      68,    69,    70,    72,    73,    74,    75,    78,    79,    87,
      91,    41,    42,    44,    44,    44,    69,    70,    71,    91,
      22,    23,    24,    25,    26,    27,    31,    37,    44,    76,
      77,    79,    83,    86,    87,    89,    90,    92,    83,    68,
      48,    41,    41,    49,    42,    44,    56,    90,    64,    65,
      88,    83,    83,    11,    83,    83,    83,    46,    16,    17,
      18,    19,    20,    21,    30,    31,    32,    33,    35,    36,
      84,    85,    48,    68,    68,    83,    83,    80,    55,    43,
      45,    40,    45,    45,    44,    45,    76,    83,    83,    43,
      81,    82,    83,    91,     9,    12,    83,    45,    46,    46,
      71,    71,    45,    82,    66,    10,    65,    71
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}

/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*-------------------------.
| yyparse or yypush_parse.  |
`-------------------------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{


    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:

/* Line 1455 of yacc.c  */
#line 131 "parser.y"
    {tabela_simbolos_escopo = escopo_criaEscopoGlobal();;}
    break;

  case 3:

/* Line 1455 of yacc.c  */
#line 132 "parser.y"
    { 
					id_nodo++; 
					arvore = arvore_cria_nodo_pai(IKS_AST_PROGRAMA,NULL,id_nodo,IKS_TIPO_ESPECIAL,0,(comp_tree_t*)(yyvsp[(2) - (2)].no),NULL,NULL,NULL);
					arvore->tipo = arvore_inferencia_de_tipo(arvore->filhos);
					return retorno; 
				;}
    break;

  case 4:

/* Line 1455 of yacc.c  */
#line 139 "parser.y"
    { (yyval.no) = (yyvsp[(2) - (2)].no); retorno = IKS_SYNTAX_SUCESSO;;}
    break;

  case 5:

/* Line 1455 of yacc.c  */
#line 140 "parser.y"
    { (yyval.no) = NULL; retorno = IKS_SYNTAX_SUCESSO; ;}
    break;

  case 6:

/* Line 1455 of yacc.c  */
#line 141 "parser.y"
    { (yyval.no) = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*)(yyvsp[(1) - (2)].no), (comp_tree_t*)(yyvsp[(2) - (2)].no));
						  retorno = IKS_SYNTAX_SUCESSO; 
						;}
    break;

  case 7:

/* Line 1455 of yacc.c  */
#line 144 "parser.y"
    { yyerrok; yyclearin; retorno = IKS_SYNTAX_ERRO; return IKS_SYNTAX_ERRO;;}
    break;

  case 8:

/* Line 1455 of yacc.c  */
#line 149 "parser.y"
    {	tabela_simbolos_escopo = escopo_insereEscopoGlobal(tabela_simbolos_escopo,(comp_dict_t*) (yyvsp[(3) - (4)].tabela));
			 	if(tabela_simbolos_escopo==NULL) {Imprime_Erro(linha,IKS_ERROR_DECLARED); return IKS_ERROR_DECLARED;}
			;}
    break;

  case 9:

/* Line 1455 of yacc.c  */
#line 155 "parser.y"
    {	comp_tree_t* arv = (comp_tree_t*) (yyvsp[(3) - (4)].no);
					tabela_simbolos_escopo = escopo_insereEscopoLocal(tabela_simbolos_escopo,arv->entrada); 
					if(tabela_simbolos_escopo==NULL) {Imprime_Erro(linha,IKS_ERROR_DECLARED); return IKS_ERROR_DECLARED;} 
				;}
    break;

  case 10:

/* Line 1455 of yacc.c  */
#line 158 "parser.y"
    {(yyval.no)=NULL;;}
    break;

  case 11:

/* Line 1455 of yacc.c  */
#line 159 "parser.y"
    {(yyval.no) = NULL;;}
    break;

  case 12:

/* Line 1455 of yacc.c  */
#line 161 "parser.y"
    {comp_tree_t* arv = (comp_tree_t*) (yyvsp[(1) - (2)].no); 
						if((yyvsp[(2) - (2)].vetorTam)!=0) (yyval.tabela) = (struct comp_dict_t*) escopo_defineVetor((yyvsp[(2) - (2)].vetorTam),arv->entrada);
						else (yyval.tabela) = (struct comp_dict_t*) arv->entrada;
					;}
    break;

  case 13:

/* Line 1455 of yacc.c  */
#line 165 "parser.y"
    {(yyval.vetorTam) = 0;;}
    break;

  case 14:

/* Line 1455 of yacc.c  */
#line 166 "parser.y"
    {nodo_nao_valido = 1;;}
    break;

  case 15:

/* Line 1455 of yacc.c  */
#line 166 "parser.y"
    {nodo_nao_valido = 0;  comp_tree_t* arv = (comp_tree_t*) (yyvsp[(3) - (4)].no); (yyval.vetorTam) = atoi(arv->entrada->chave);}
    break;

  case 16:

/* Line 1455 of yacc.c  */
#line 169 "parser.y"
    {declaracao_id=0;;}
    break;

  case 17:

/* Line 1455 of yacc.c  */
#line 169 "parser.y"
    { id_nodo++; (yyval.no) = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*)(yyvsp[(1) - (4)].no),(comp_tree_t*)(yyvsp[(4) - (4)].no));declaracao_id=1;;}
    break;

  case 18:

/* Line 1455 of yacc.c  */
#line 172 "parser.y"
    {
				tabela_simbolos_escopo = escopo_criaEscopoLocal(tabela_simbolos_escopo);
				funcao_tipo_retorno = (yyvsp[(1) - (3)].tipo); //salva tipo da função atual no momento que ocorrer return
				comp_tree_t* idf = (comp_tree_t*)(yyvsp[(3) - (3)].no); 
				tabela_simbolos_escopo = escopo_insereFuncaoEscopoGlobal(tabela_simbolos_escopo,idf->entrada,funcao_tipo_retorno);
				if(tabela_simbolos_escopo==NULL) {Imprime_Erro(linha,IKS_ERROR_DECLARED); return IKS_ERROR_DECLARED;} 
				
			;}
    break;

  case 19:

/* Line 1455 of yacc.c  */
#line 181 "parser.y"
    { 	
				comp_tree_t* idf = (comp_tree_t*)(yyvsp[(3) - (7)].no); (yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_FUNCAO,idf->entrada,NULL,idf->id,idf->tipo, idf->tamanho); 
			;}
    break;

  case 20:

/* Line 1455 of yacc.c  */
#line 184 "parser.y"
    {(yyval.no) = NULL;;}
    break;

  case 21:

/* Line 1455 of yacc.c  */
#line 184 "parser.y"
    {(yyval.no)=(yyvsp[(1) - (1)].no);;}
    break;

  case 22:

/* Line 1455 of yacc.c  */
#line 186 "parser.y"
    {	comp_tree_t* arv = (comp_tree_t*) (yyvsp[(3) - (4)].no); tabela_simbolos_escopo = escopo_insereEscopoLocal(tabela_simbolos_escopo,arv->entrada);
				if(tabela_simbolos_escopo==NULL) {Imprime_Erro(linha,IKS_ERROR_DECLARED); return IKS_ERROR_DECLARED;} 
				tabela_simbolos_escopo = escopo_insereParametrosFuncao(tabela_simbolos_escopo,(yyvsp[(1) - (4)].tipo));
			;}
    break;

  case 23:

/* Line 1455 of yacc.c  */
#line 189 "parser.y"
    {(yyval.no) = NULL;;}
    break;

  case 24:

/* Line 1455 of yacc.c  */
#line 191 "parser.y"
    {	comp_tree_t* arv = (comp_tree_t*) (yyvsp[(3) - (3)].no); tabela_simbolos_escopo = escopo_insereEscopoLocal(tabela_simbolos_escopo,arv->entrada);
					if(tabela_simbolos_escopo==NULL) {Imprime_Erro(linha,IKS_ERROR_DECLARED); return IKS_ERROR_DECLARED;} 
					tabela_simbolos_escopo = escopo_insereParametrosFuncao(tabela_simbolos_escopo,(yyvsp[(1) - (3)].tipo));
				;}
    break;

  case 25:

/* Line 1455 of yacc.c  */
#line 197 "parser.y"
    {(yyval.no) = (yyvsp[(2) - (3)].no);;}
    break;

  case 26:

/* Line 1455 of yacc.c  */
#line 199 "parser.y"
    {id_nodo++; (yyval.no) = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*)(yyvsp[(1) - (3)].no),(comp_tree_t*)(yyvsp[(3) - (3)].no));;}
    break;

  case 27:

/* Line 1455 of yacc.c  */
#line 200 "parser.y"
    {id_nodo++;(yyval.no) = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*) (yyvsp[(1) - (3)].no),(comp_tree_t*)(yyvsp[(3) - (3)].no));;}
    break;

  case 28:

/* Line 1455 of yacc.c  */
#line 201 "parser.y"
    {(yyval.no) = NULL;;}
    break;

  case 29:

/* Line 1455 of yacc.c  */
#line 202 "parser.y"
    {(yyval.no) = NULL; ;}
    break;

  case 30:

/* Line 1455 of yacc.c  */
#line 203 "parser.y"
    {id_nodo++;(yyval.no) = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*)(yyvsp[(1) - (1)].no),NULL);;}
    break;

  case 31:

/* Line 1455 of yacc.c  */
#line 204 "parser.y"
    {id_nodo++;(yyval.no) = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*)(yyvsp[(1) - (1)].no),NULL);;}
    break;

  case 32:

/* Line 1455 of yacc.c  */
#line 206 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 33:

/* Line 1455 of yacc.c  */
#line 207 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 34:

/* Line 1455 of yacc.c  */
#line 208 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 35:

/* Line 1455 of yacc.c  */
#line 209 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 36:

/* Line 1455 of yacc.c  */
#line 210 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 37:

/* Line 1455 of yacc.c  */
#line 211 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 38:

/* Line 1455 of yacc.c  */
#line 213 "parser.y"
    {id_nodo++; comp_tree_t* arv = (comp_tree_t*) (yyvsp[(2) - (3)].no); int tipo = 0; if(arv!=NULL) tipo = arv->tipo; 
					(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo_pai(IKS_AST_BLOCO,NULL,id_nodo,tipo,0,arv,NULL,NULL,NULL);;}
    break;

  case 39:

/* Line 1455 of yacc.c  */
#line 216 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 40:

/* Line 1455 of yacc.c  */
#line 217 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 41:

/* Line 1455 of yacc.c  */
#line 220 "parser.y"
    {
						id_nodo++;
						comp_tree_t* arv = (comp_tree_t*) (yyvsp[(1) - (3)].no);
						comp_tree_t* arvC = (comp_tree_t*) (yyvsp[(3) - (3)].no);
						int tipo_convertido = arvC->tipo;
						arvC = arvore_coercao(arvC,arv->tipo);
						if(arvC==NULL) {Imprime_Erro(linha,tipo_coercao_incompativel(tipo_convertido)); return tipo_coercao_incompativel(tipo_convertido);}
						(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo_pai(IKS_AST_ATRIBUICAO,NULL,id_nodo,arv->tipo,0,(comp_tree_t*) (yyvsp[(1) - (3)].no),(comp_tree_t*) (yyvsp[(3) - (3)].no),NULL,NULL);				
						
					   ;}
    break;

  case 42:

/* Line 1455 of yacc.c  */
#line 232 "parser.y"
    {	comp_tree_t* arvC = (comp_tree_t*) (yyvsp[(3) - (6)].no);
					arvC = arvore_coercao(arvC,IKS_BOOL);
					if(arvC==NULL) {Imprime_Erro(linha,IKS_ERROR_COND_NOT_BOOL); return tipo_coercao_incompativel(IKS_ERROR_COND_NOT_BOOL);}
					id_nodo++; comp_tree_t* arv = arvore_cria_nodo_pai(IKS_AST_IF_ELSE ,NULL,id_nodo,IKS_TIPO_ESPECIAL,0,arvC,(comp_tree_t*) (yyvsp[(6) - (6)].no),NULL,NULL);
					arv->tipo = arvore_inferencia_de_tipo(arv->filhos);	
					(yyval.no) = (struct comp_tree_t*) arv;
				;}
    break;

  case 43:

/* Line 1455 of yacc.c  */
#line 240 "parser.y"
    { 	comp_tree_t* arvC = (comp_tree_t*) (yyvsp[(3) - (8)].no);
					arvC = arvore_coercao(arvC,IKS_BOOL);
					if(arvC==NULL) {Imprime_Erro(linha,IKS_ERROR_COND_NOT_BOOL); return tipo_coercao_incompativel(IKS_ERROR_COND_NOT_BOOL);}
					id_nodo++; comp_tree_t* arv = arvore_cria_nodo_pai(IKS_AST_IF_ELSE ,NULL,id_nodo,IKS_TIPO_ESPECIAL,0,arvC,(comp_tree_t*) (yyvsp[(6) - (8)].no),(comp_tree_t*) (yyvsp[(8) - (8)].no),NULL);
					arv->tipo = arvore_inferencia_de_tipo(arv->filhos);	
					(yyval.no) = (struct comp_tree_t*) arv;			
				;}
    break;

  case 44:

/* Line 1455 of yacc.c  */
#line 248 "parser.y"
    {	comp_tree_t* arvC = (comp_tree_t*) (yyvsp[(3) - (6)].no);
					arvC = arvore_coercao(arvC,IKS_BOOL);
					if(arvC==NULL) {Imprime_Erro(linha,IKS_ERROR_COND_NOT_BOOL); return tipo_coercao_incompativel(IKS_ERROR_COND_NOT_BOOL);}
					id_nodo++; comp_tree_t* arv = arvore_cria_nodo_pai(IKS_AST_WHILE_DO ,NULL,id_nodo,IKS_TIPO_ESPECIAL,0,arvC,(comp_tree_t*) (yyvsp[(6) - (6)].no),NULL,NULL);
					arv->tipo = arvore_inferencia_de_tipo(arv->filhos);	
					(yyval.no) = (struct comp_tree_t*) arv;
				;}
    break;

  case 45:

/* Line 1455 of yacc.c  */
#line 256 "parser.y"
    {	comp_tree_t* arvC = (comp_tree_t*) (yyvsp[(5) - (6)].no);
					arvC = arvore_coercao(arvC,IKS_BOOL);
					if(arvC==NULL) {Imprime_Erro(linha,IKS_ERROR_COND_NOT_BOOL); return tipo_coercao_incompativel(IKS_ERROR_COND_NOT_BOOL);}
					id_nodo++; comp_tree_t* arv = arvore_cria_nodo_pai(IKS_AST_DO_WHILE ,NULL,id_nodo,IKS_TIPO_ESPECIAL,0,(comp_tree_t*) (yyvsp[(2) - (6)].no),arvC,NULL,NULL);
					arv->tipo = arvore_inferencia_de_tipo(arv->filhos);	
					(yyval.no) = (struct comp_tree_t*) arv;
				;}
    break;

  case 46:

/* Line 1455 of yacc.c  */
#line 265 "parser.y"
    {
				id_nodo++;
				comp_tree_t* id = (comp_tree_t*) (yyvsp[(2) - (2)].no);
				comp_tree_t* id_arv = arvore_cria_nodo(IKS_AST_IDENTIFICADOR,id->entrada,NULL,id->id,id->tipo,id->tamanho);
				
				(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo_pai(IKS_AST_INPUT ,NULL,id_nodo,id_arv->tipo,0,id_arv,NULL,NULL,NULL);
									
			;}
    break;

  case 47:

/* Line 1455 of yacc.c  */
#line 275 "parser.y"
    { 
			  	id_nodo++;
				comp_tree_t* arv = (comp_tree_t*) (yyvsp[(2) - (2)].no);
				(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo_pai(IKS_AST_OUTPUT ,NULL,id_nodo,arv->tipo,0,(comp_tree_t*) (yyvsp[(2) - (2)].no),NULL,NULL,NULL);
				
			;}
    break;

  case 48:

/* Line 1455 of yacc.c  */
#line 282 "parser.y"
    {
				id_nodo++;
				(yyval.no) = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*)(yyvsp[(1) - (3)].no),(comp_tree_t*) (yyvsp[(3) - (3)].no));
				
			;}
    break;

  case 49:

/* Line 1455 of yacc.c  */
#line 287 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 50:

/* Line 1455 of yacc.c  */
#line 288 "parser.y"
    {(yyval.no)=(yyvsp[(1) - (1)].no);;}
    break;

  case 51:

/* Line 1455 of yacc.c  */
#line 291 "parser.y"
    {
				id_nodo++;
				comp_tree_t* arv = (comp_tree_t*) (yyvsp[(2) - (2)].no);	
				int tipo_convertido = arv->tipo;
				arv = arvore_coercao(arv,funcao_tipo_retorno);
				if(arv==NULL) {Imprime_Erro(linha,IKS_ERROR_WRONG_PAR_RETURN); return IKS_ERROR_WRONG_PAR_RETURN;}
				(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo_pai(IKS_AST_RETURN ,NULL,id_nodo,arv->tipo,0,arv,NULL,NULL,NULL);
									
			;}
    break;

  case 52:

/* Line 1455 of yacc.c  */
#line 301 "parser.y"
    {  	comp_tree_t* id = (comp_tree_t*) (yyvsp[(1) - (2)].no);
					if(id->entrada->tipo_id==TIPO_ID_VETOR)
						{Imprime_Erro(linha,IKS_ERROR_VECTOR); return IKS_ERROR_VECTOR;}
					else if(id->entrada->tipo_id == TIPO_ID_VARIAVEL)
						{Imprime_Erro(linha,IKS_ERROR_VARIABLE); return IKS_ERROR_VARIABLE;}
				    ;}
    break;

  case 53:

/* Line 1455 of yacc.c  */
#line 307 "parser.y"
    {
				id_nodo++;
				comp_tree_t* id = (comp_tree_t*) (yyvsp[(1) - (5)].no);
				comp_tree_t* passagem = (comp_tree_t*) (yyvsp[(4) - (5)].no);
				
				int passagem_confere = confere_passagem_funcao(tabela_simbolos_escopo->escopo_global,id->entrada->chave,passagem); 
				if(passagem_confere!=0) { Imprime_Erro(linha,passagem_confere); return passagem_confere;}
				comp_tree_t* id_arv = arvore_cria_nodo(IKS_AST_IDENTIFICADOR,id->entrada,NULL,id->id,id->tipo,id->tamanho);
				int tipo_funcao = escopo_retornoFuncao(tabela_simbolos_escopo,id->entrada->chave);
				(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo_pai(IKS_AST_CHAMADA_DE_FUNCAO ,NULL,id_nodo,tipo_funcao,0,id_arv,passagem,NULL,NULL);
			;}
    break;

  case 54:

/* Line 1455 of yacc.c  */
#line 318 "parser.y"
    {(yyval.no) = NULL;;}
    break;

  case 55:

/* Line 1455 of yacc.c  */
#line 318 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 56:

/* Line 1455 of yacc.c  */
#line 320 "parser.y"
    {
				id_nodo++;
				(yyval.no) = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*) (yyvsp[(1) - (3)].no),(comp_tree_t*) (yyvsp[(3) - (3)].no));
				
				
			;}
    break;

  case 57:

/* Line 1455 of yacc.c  */
#line 326 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 58:

/* Line 1455 of yacc.c  */
#line 328 "parser.y"
    {(yyval.no) = (yyvsp[(2) - (3)].no);;}
    break;

  case 59:

/* Line 1455 of yacc.c  */
#line 330 "parser.y"
    { 
					id_nodo++;					
					comp_tree_t* arv = arvore_cria_nodo_pai((yyvsp[(2) - (3)].ast_op),NULL,id_nodo,IKS_TIPO_ESPECIAL,0,(comp_tree_t*) (yyvsp[(1) - (3)].no),(comp_tree_t*) (yyvsp[(3) - (3)].no),NULL,NULL);
					arv->tipo = arvore_inferencia_de_tipo(arv->filhos);	
					(yyval.no) = (struct comp_tree_t*)arv;	
				;}
    break;

  case 60:

/* Line 1455 of yacc.c  */
#line 337 "parser.y"
    { 
					id_nodo++;					
					comp_tree_t* arv = arvore_cria_nodo_pai((yyvsp[(2) - (3)].ast_op),NULL,id_nodo,IKS_TIPO_ESPECIAL,0,(comp_tree_t*) (yyvsp[(1) - (3)].no),(comp_tree_t*) (yyvsp[(3) - (3)].no),NULL,NULL);
					arv->tipo = IKS_BOOL;	
					(yyval.no) = (struct comp_tree_t*) arv;	
				;}
    break;

  case 61:

/* Line 1455 of yacc.c  */
#line 344 "parser.y"
    { 
					id_nodo++;
					comp_tree_t* arv = (comp_tree_t*) (yyvsp[(2) - (2)].no);			
					(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo_pai(IKS_AST_ARIM_INVERSAO,NULL,id_nodo,arv->tipo,0,(comp_tree_t*) (yyvsp[(2) - (2)].no),NULL,NULL,NULL);	
				;}
    break;

  case 62:

/* Line 1455 of yacc.c  */
#line 350 "parser.y"
    { 
					id_nodo++;	
					comp_tree_t* arv = (comp_tree_t*) (yyvsp[(2) - (2)].no);					
					(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo_pai(IKS_AST_LOGICO_COMP_NEGACAO,NULL,id_nodo,IKS_BOOL,0,(comp_tree_t*) (yyvsp[(2) - (2)].no),NULL,NULL,NULL);	
				;}
    break;

  case 63:

/* Line 1455 of yacc.c  */
#line 355 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 64:

/* Line 1455 of yacc.c  */
#line 357 "parser.y"
    { (yyval.ast_op) =  IKS_AST_ARIM_SOMA; ;}
    break;

  case 65:

/* Line 1455 of yacc.c  */
#line 358 "parser.y"
    { (yyval.ast_op) =  IKS_AST_ARIM_SUBTRACAO; ;}
    break;

  case 66:

/* Line 1455 of yacc.c  */
#line 359 "parser.y"
    { (yyval.ast_op) =  IKS_AST_ARIM_MULTIPLICACAO; ;}
    break;

  case 67:

/* Line 1455 of yacc.c  */
#line 360 "parser.y"
    { (yyval.ast_op) =  IKS_AST_ARIM_DIVISAO; ;}
    break;

  case 68:

/* Line 1455 of yacc.c  */
#line 362 "parser.y"
    { (yyval.ast_op) =  IKS_AST_LOGICO_OU; ;}
    break;

  case 69:

/* Line 1455 of yacc.c  */
#line 363 "parser.y"
    { (yyval.ast_op) =  IKS_AST_LOGICO_E; ;}
    break;

  case 70:

/* Line 1455 of yacc.c  */
#line 364 "parser.y"
    { (yyval.ast_op) =  IKS_AST_LOGICO_COMP_LE; ;}
    break;

  case 71:

/* Line 1455 of yacc.c  */
#line 365 "parser.y"
    { (yyval.ast_op) =  IKS_AST_LOGICO_COMP_GE; ;}
    break;

  case 72:

/* Line 1455 of yacc.c  */
#line 366 "parser.y"
    { (yyval.ast_op) =  IKS_AST_LOGICO_COMP_IGUAL; ;}
    break;

  case 73:

/* Line 1455 of yacc.c  */
#line 367 "parser.y"
    { (yyval.ast_op) =  IKS_AST_LOGICO_COMP_DIF; ;}
    break;

  case 74:

/* Line 1455 of yacc.c  */
#line 368 "parser.y"
    { (yyval.ast_op) =  IKS_AST_LOGICO_COMP_L; ;}
    break;

  case 75:

/* Line 1455 of yacc.c  */
#line 369 "parser.y"
    { (yyval.ast_op) =  IKS_AST_LOGICO_COMP_G; ;}
    break;

  case 76:

/* Line 1455 of yacc.c  */
#line 371 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 77:

/* Line 1455 of yacc.c  */
#line 372 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 78:

/* Line 1455 of yacc.c  */
#line 373 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 79:

/* Line 1455 of yacc.c  */
#line 375 "parser.y"
    { 	comp_tree_t* id = (comp_tree_t*) (yyvsp[(1) - (1)].no);

					if(id->entrada->tipo_id==TIPO_ID_VETOR)
						{Imprime_Erro(linha,IKS_ERROR_VECTOR); return IKS_ERROR_VECTOR;}
					else if(id->entrada->tipo_id == TIPO_ID_FUNCAO)
						{Imprime_Erro(linha,IKS_ERROR_FUNCTION); return IKS_ERROR_FUNCTION;}

					(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_IDENTIFICADOR,id->entrada,NULL,id->id,id->tipo, id->tamanho);;}
    break;

  case 80:

/* Line 1455 of yacc.c  */
#line 384 "parser.y"
    { 
					comp_tree_t* id = (comp_tree_t*) (yyvsp[(1) - (4)].no);					
					id_nodo++;					
					if(id->entrada->tipo_id==TIPO_ID_VARIAVEL)
						{Imprime_Erro(linha,IKS_ERROR_VARIABLE); return IKS_ERROR_VARIABLE;}
					else if(id->entrada->tipo_id == TIPO_ID_FUNCAO)
						{Imprime_Erro(linha,IKS_ERROR_FUNCTION); return IKS_ERROR_FUNCTION;}
					
					comp_tree_t* arvC = (comp_tree_t*) (yyvsp[(3) - (4)].no);
					int tipo_convertido = arvC->tipo;
					arvC = arvore_coercao(arvC,IKS_INT);
					if(arvC==NULL) {Imprime_Erro(linha,tipo_coercao_incompativel(tipo_convertido)); return tipo_coercao_incompativel(tipo_convertido);}
					comp_tree_t* id_arv = arvore_cria_nodo(IKS_AST_IDENTIFICADOR,id->entrada,NULL,id->id,id->tipo,id->tamanho);
					
					(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo_pai(IKS_AST_VETOR_INDEXADO,NULL,id_nodo,id_arv->tipo,0,id_arv,(comp_tree_t*) (yyvsp[(3) - (4)].no),NULL,NULL);
				;}
    break;

  case 81:

/* Line 1455 of yacc.c  */
#line 403 "parser.y"
    { tipo_id = IKS_INT;tamanho_id=tamanho_int; (yyval.tipo) = IKS_INT;;}
    break;

  case 82:

/* Line 1455 of yacc.c  */
#line 404 "parser.y"
    { tipo_id = IKS_FLOAT;tamanho_id=tamanho_float; (yyval.tipo) = IKS_FLOAT;;}
    break;

  case 83:

/* Line 1455 of yacc.c  */
#line 405 "parser.y"
    { tipo_id = IKS_BOOL;tamanho_id=tamanho_bool; (yyval.tipo)=IKS_BOOL;;}
    break;

  case 84:

/* Line 1455 of yacc.c  */
#line 406 "parser.y"
    { tipo_id = IKS_CHAR;tamanho_id=tamanho_char; (yyval.tipo)=IKS_CHAR;;}
    break;

  case 85:

/* Line 1455 of yacc.c  */
#line 407 "parser.y"
    { tipo_id = IKS_STRING;tamanho_id=tamanho_char;(yyval.tipo) = IKS_STRING;;}
    break;

  case 86:

/* Line 1455 of yacc.c  */
#line 409 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 87:

/* Line 1455 of yacc.c  */
#line 410 "parser.y"
    { dicionario = dicionario_AdicionaTipo(yytext,IKS_FLOAT,tamanho_float,dicionario);
					  comp_dict_t* entrada = dicionario_LocalizarEntrada(yytext, dicionario);
					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  (yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo,IKS_FLOAT,tamanho_float);					  
					;}
    break;

  case 88:

/* Line 1455 of yacc.c  */
#line 416 "parser.y"
    { dicionario = dicionario_AdicionaTipo(sem_primeiro,IKS_CHAR,tamanho_char,dicionario);
				          comp_dict_t* entrada = dicionario_LocalizarEntrada(sem_primeiro, dicionario);
					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  (yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo,IKS_CHAR,tamanho_char);
					;}
    break;

  case 89:

/* Line 1455 of yacc.c  */
#line 422 "parser.y"
    { (yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 90:

/* Line 1455 of yacc.c  */
#line 423 "parser.y"
    { dicionario = dicionario_AdicionaTipo(yytext,IKS_BOOL,tamanho_bool,dicionario);
					  comp_dict_t* entrada = dicionario_LocalizarEntrada(yytext, dicionario);
					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  (yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo,IKS_BOOL,tamanho_bool);
					;}
    break;

  case 91:

/* Line 1455 of yacc.c  */
#line 429 "parser.y"
    { dicionario = dicionario_AdicionaTipo(yytext,IKS_BOOL,tamanho_bool,dicionario);
					  comp_dict_t* entrada = dicionario_LocalizarEntrada(yytext, dicionario);
					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  (yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo,IKS_BOOL,tamanho_bool);
					;}
    break;

  case 92:

/* Line 1455 of yacc.c  */
#line 435 "parser.y"
    { dicionario = dicionario_AdicionaTipo(yytext,IKS_INT,tamanho_int,dicionario);
					  comp_dict_t* entrada = dicionario_LocalizarEntrada(yytext, dicionario);
					  int ast_id =  IKS_AST_LITERAL;
					  valor_vetor_id = atoi(yytext);
					  if(nodo_nao_valido==0)
					  {
						id_nodo++;
				   	  	(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo,IKS_INT,tamanho_int);
					  }
					  else { (yyval.no) = (struct comp_tree_t*) arvore_cria_nodo_inutil(entrada,id_nodo,IKS_INT,tamanho_int);}
					
					;}
    break;

  case 93:

/* Line 1455 of yacc.c  */
#line 448 "parser.y"
    { 
						ultimo_id = (char*)malloc(strlen(yytext)); strcpy(ultimo_id,yytext);	   						
						if(declaracao_id==1)
						{ 
							comp_dict_t* entrada = dicionario_Criar_Entrada(dicionario_LocalizarEntrada(yytext, dicionario));
							id_nodo++;
							(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo_inutil(entrada,id_nodo,tipo_id,tamanho_id);
						}
						else
						{
							comp_dict_t* entrada = dicionario_Criar_Entrada(escopo_localizarEm_LocalAtual_Global(tabela_simbolos_escopo,yytext));
							if(entrada==NULL) { Imprime_Erro(linha,IKS_ERROR_UNDECLARED); return IKS_ERROR_UNDECLARED;}
									   
							id_nodo++;
							(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo_inutil(entrada,id_nodo,entrada->tipo,entrada->tamanho);
						}
				   	 ;}
    break;

  case 94:

/* Line 1455 of yacc.c  */
#line 465 "parser.y"
    {   					  
					  dicionario = dicionario_AdicionaTipo(sem_primeiro,IKS_STRING,tam_string*tamanho_char,dicionario);
						
					  comp_dict_t* entrada = dicionario_LocalizarEntrada(sem_primeiro, dicionario);

					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  (yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo,IKS_STRING,tam_string*tamanho_char);

					;}
    break;



/* Line 1455 of yacc.c  */
#line 2415 "/home/aluno/compiladores-etapa4/build/parser.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 1675 of yacc.c  */
#line 478 "parser.y"




