%{
/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include <string.h>
#include "main.h"

//tamanho em bytes de cada tipo (etapa4)
#define tamanho_int 4
#define tamanho_float 8
#define tamanho_char 1
#define tamanho_bool 1
//string = tamanho char*nºcarac
//vetor = tamanho*tipo

extern int linha;
extern comp_dict_t* dicionario;
extern char* yytext;
extern char* sem_primeiro;
extern int tam_string;
extern comp_tree_t* arvore;
extern escopo_tree_t* tabela_simbolos_escopo;
comp_dict_t* simbolo;
int tipo_id;
int tamanho_id;
int id_nodo=0;
int retorno;
int nodo_nao_valido = 0;
int declaracao_id = 1;
char* ultimo_id;
int valor_vetor_id;
int funcao_tipo_retorno = -1; //variavel para salvar o tipo de retorno da função em cursão (para converir return)
char * ultimo_id_funcao; //variavel para salvar id da função para adicionar parametros (cabeçalho)
comp_dict_t* ultimo_simbolo_funcao;
%}

%union
{	
	struct comp_tree_t* no;
	int ast_op;
	int tipo;
	struct comp_dict_t* tabela;
	int vetorTam;
}
/* Declaração dos tokens da gramática da Linguagem K */
%token<no> TK_PR_INT
%token<no> TK_PR_FLOAT
%token<no> TK_PR_BOOL
%token<no> TK_PR_CHAR
%token<no> TK_PR_STRING
%token<no> TK_PR_IF
%token<no> TK_PR_THEN
%token<no> TK_PR_ELSE
%token<no> TK_PR_WHILE
%token<no> TK_PR_DO
%token<no> TK_PR_INPUT
%token<no> TK_PR_OUTPUT
%token<no> TK_PR_RETURN
%token<no> TK_OC_LE
%token<no> TK_OC_GE
%token<no> TK_OC_EQ
%token<no> TK_OC_NE
%token<no> TK_OC_AND
%token<no> TK_OC_OR
%token<no> TK_LIT_INT
%token<no> TK_LIT_FLOAT
%token<no> TK_LIT_FALSE
%token<no> TK_LIT_TRUE
%token<no> TK_LIT_CHAR
%token<no> TK_LIT_STRING
%token<no> TK_IDENTIFICADOR
%token<no> TOKEN_ERRO


%type<no> func
%type<no> com_s
%type<no> atrib
%type<no> corpo
%type<no> seq_com
%type<no> prog
%type<no> s

%type<no> decl_global
%type<tabela> acesso_id_esp
%type<vetorTam> vetor_esp
%type<no> cab
%type<no> param
%type<no> lst_param
%type<no> decl_local
%type<no> com_b
%type<no> com_u
%type<no> fluxo
%type<no> entrada
%type<no> saida
%type<no> lst_saida
%type<no> elem_saida
%type<no> retorno
%type<no> f_cham
%type<no> passag
%type<no> lst_p
%type<no> expr
%type<ast_op> opArit
%type<ast_op> opLog
%type<no> elemento
%type<no> acesso_id
%type<tipo> tipo
%type<no> tipo_lit
%type<no> lit_int_pos
%type<no> tipo_id
%type<no> tipo_string


%start s


%left '+' '-' 
%left '*' '/'
%left UMINUS
%left TK_OC_OR TK_OC_AND TK_OC_LE TK_OC_GE TK_OC_EQ TK_OC_NE '<' '>' '!'
%left TK_LIT_STRING
%right TK_PR_ELSE TK_PR_THEN
%right "expr"
%right "op"

%%
 /* Regras (e ações) da gramática da Linguagem K */

s		:	{tabela_simbolos_escopo = escopo_criaEscopoGlobal();} 
			prog	{ 
					id_nodo++; 
					arvore = arvore_cria_nodo_pai(IKS_AST_PROGRAMA,NULL,id_nodo,IKS_TIPO_ESPECIAL,0,(comp_tree_t*)$2,NULL,NULL,NULL);
					arvore->tipo = arvore_inferencia_de_tipo(arvore->filhos);
					return retorno; 
				};
			
prog		:	  decl_global prog 	{ $$ = $2; retorno = IKS_SYNTAX_SUCESSO;}
			| /* empty */ 		{ $$ = NULL; retorno = IKS_SYNTAX_SUCESSO; }
			| func prog		{ $$ = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*)$1, (comp_tree_t*)$2);
						  retorno = IKS_SYNTAX_SUCESSO; 
						}
			| error 		{ yyerrok; yyclearin; retorno = IKS_SYNTAX_ERRO; return IKS_SYNTAX_ERRO;};
			

//declarações
decl_global	:	tipo ':' acesso_id_esp ';' 
			{	tabela_simbolos_escopo = escopo_insereEscopoGlobal(tabela_simbolos_escopo,(comp_dict_t*) $3);
			 	if(tabela_simbolos_escopo==NULL) {Imprime_Erro(linha,IKS_ERROR_DECLARED); return IKS_ERROR_DECLARED;}
			};


decl_local	:	tipo ':' tipo_id ';' 
				{	comp_tree_t* arv = (comp_tree_t*) $3;
					tabela_simbolos_escopo = escopo_insereEscopoLocal(tabela_simbolos_escopo,arv->entrada); 
					if(tabela_simbolos_escopo==NULL) {Imprime_Erro(linha,IKS_ERROR_DECLARED); return IKS_ERROR_DECLARED;} 
				} decl_local {$$=NULL;}
			| /* empty */ {$$ = NULL;};

acesso_id_esp	:	tipo_id vetor_esp {comp_tree_t* arv = (comp_tree_t*) $1; 
						if($2!=0) $$ = (struct comp_dict_t*) escopo_defineVetor($2,arv->entrada);
						else $$ = (struct comp_dict_t*) arv->entrada;
					};
vetor_esp	:	/* empty */ {$$ = 0;} 
			| {nodo_nao_valido = 1;}'[' lit_int_pos ']'{nodo_nao_valido = 0;  comp_tree_t* arv = (comp_tree_t*) $3; $$ = atoi(arv->entrada->chave)};


func		:	cab decl_local {declaracao_id=0;} corpo { id_nodo++; $$ = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*)$1,(comp_tree_t*)$4);declaracao_id=1;} ;

cab		:	tipo ':' tipo_id  
			{
				tabela_simbolos_escopo = escopo_criaEscopoLocal(tabela_simbolos_escopo);
				funcao_tipo_retorno = $1; //salva tipo da função atual no momento que ocorrer return
				comp_tree_t* idf = (comp_tree_t*)$3; 
				tabela_simbolos_escopo = escopo_insereFuncaoEscopoGlobal(tabela_simbolos_escopo,idf->entrada,funcao_tipo_retorno);
				if(tabela_simbolos_escopo==NULL) {Imprime_Erro(linha,IKS_ERROR_DECLARED); return IKS_ERROR_DECLARED;} 
				
			}
			'(' param ')' 
			{ 	
				comp_tree_t* idf = (comp_tree_t*)$3; $$ = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_FUNCAO,idf->entrada,NULL,idf->id,idf->tipo, idf->tamanho); 
			} ;
param		:	/* empty */ {$$ = NULL;} | lst_param {$$=$1;} ;
lst_param	:	tipo ':' tipo_id ',' 
			{	comp_tree_t* arv = (comp_tree_t*) $3; tabela_simbolos_escopo = escopo_insereEscopoLocal(tabela_simbolos_escopo,arv->entrada);
				if(tabela_simbolos_escopo==NULL) {Imprime_Erro(linha,IKS_ERROR_DECLARED); return IKS_ERROR_DECLARED;} 
				tabela_simbolos_escopo = escopo_insereParametrosFuncao(tabela_simbolos_escopo,$1);
			} lst_param {$$ = NULL;}
			| tipo ':' tipo_id 
				{	comp_tree_t* arv = (comp_tree_t*) $3; tabela_simbolos_escopo = escopo_insereEscopoLocal(tabela_simbolos_escopo,arv->entrada);
					if(tabela_simbolos_escopo==NULL) {Imprime_Erro(linha,IKS_ERROR_DECLARED); return IKS_ERROR_DECLARED;} 
					tabela_simbolos_escopo = escopo_insereParametrosFuncao(tabela_simbolos_escopo,$1);
				};


corpo		:	'{' seq_com '}' {$$ = $2;} ;

seq_com		:	com_s ';' seq_com {id_nodo++; $$ = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*)$1,(comp_tree_t*)$3);} 
			| com_b ';' seq_com  {id_nodo++;$$ = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*) $1,(comp_tree_t*)$3);}
			| /* empty */ {$$ = NULL;} 
			| ';' {$$ = NULL; } 
			| com_s {id_nodo++;$$ = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*)$1,NULL);} 
			| com_b {id_nodo++;$$ = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*)$1,NULL);};

com_s		: 	  atrib {$$ = $1;} 
			| fluxo {$$ = $1;} 
			| entrada {$$ = $1;} 
			| saida {$$ = $1;} 
			| retorno {$$ = $1;} 
			| f_cham {$$ = $1;} ;

com_b		:	'{' seq_com '}' {id_nodo++; comp_tree_t* arv = (comp_tree_t*) $2; int tipo = 0; if(arv!=NULL) tipo = arv->tipo; 
					$$ = (struct comp_tree_t*) arvore_cria_nodo_pai(IKS_AST_BLOCO,NULL,id_nodo,tipo,0,arv,NULL,NULL,NULL);};

com_u		: 	  com_s {$$ = $1;} 
			| com_b {$$ = $1;} ;


atrib		:	acesso_id '=' expr {
						id_nodo++;
						comp_tree_t* arv = (comp_tree_t*) $1;
						comp_tree_t* arvC = (comp_tree_t*) $3;
						int tipo_convertido = arvC->tipo;
						arvC = arvore_coercao(arvC,arv->tipo);
						if(arvC==NULL) {Imprime_Erro(linha,tipo_coercao_incompativel(tipo_convertido)); return tipo_coercao_incompativel(tipo_convertido);}
						$$ = (struct comp_tree_t*) arvore_cria_nodo_pai(IKS_AST_ATRIBUICAO,NULL,id_nodo,arv->tipo,0,(comp_tree_t*) $1,(comp_tree_t*) $3,NULL,NULL);				
						
					   } ;

fluxo		: 	TK_PR_IF '(' expr ')' TK_PR_THEN com_u 
				{	comp_tree_t* arvC = (comp_tree_t*) $3;
					arvC = arvore_coercao(arvC,IKS_BOOL);
					if(arvC==NULL) {Imprime_Erro(linha,IKS_ERROR_COND_NOT_BOOL); return tipo_coercao_incompativel(IKS_ERROR_COND_NOT_BOOL);}
					id_nodo++; comp_tree_t* arv = arvore_cria_nodo_pai(IKS_AST_IF_ELSE ,NULL,id_nodo,IKS_TIPO_ESPECIAL,0,arvC,(comp_tree_t*) $6,NULL,NULL);
					arv->tipo = arvore_inferencia_de_tipo(arv->filhos);	
					$$ = (struct comp_tree_t*) arv;
				}
			| TK_PR_IF '(' expr ')' TK_PR_THEN com_u TK_PR_ELSE com_u 
				{ 	comp_tree_t* arvC = (comp_tree_t*) $3;
					arvC = arvore_coercao(arvC,IKS_BOOL);
					if(arvC==NULL) {Imprime_Erro(linha,IKS_ERROR_COND_NOT_BOOL); return tipo_coercao_incompativel(IKS_ERROR_COND_NOT_BOOL);}
					id_nodo++; comp_tree_t* arv = arvore_cria_nodo_pai(IKS_AST_IF_ELSE ,NULL,id_nodo,IKS_TIPO_ESPECIAL,0,arvC,(comp_tree_t*) $6,(comp_tree_t*) $8,NULL);
					arv->tipo = arvore_inferencia_de_tipo(arv->filhos);	
					$$ = (struct comp_tree_t*) arv;			
				}
			| TK_PR_WHILE '(' expr ')' TK_PR_DO com_u
				{	comp_tree_t* arvC = (comp_tree_t*) $3;
					arvC = arvore_coercao(arvC,IKS_BOOL);
					if(arvC==NULL) {Imprime_Erro(linha,IKS_ERROR_COND_NOT_BOOL); return tipo_coercao_incompativel(IKS_ERROR_COND_NOT_BOOL);}
					id_nodo++; comp_tree_t* arv = arvore_cria_nodo_pai(IKS_AST_WHILE_DO ,NULL,id_nodo,IKS_TIPO_ESPECIAL,0,arvC,(comp_tree_t*) $6,NULL,NULL);
					arv->tipo = arvore_inferencia_de_tipo(arv->filhos);	
					$$ = (struct comp_tree_t*) arv;
				}
			| TK_PR_DO com_u TK_PR_WHILE '(' expr ')' 
				{	comp_tree_t* arvC = (comp_tree_t*) $5;
					arvC = arvore_coercao(arvC,IKS_BOOL);
					if(arvC==NULL) {Imprime_Erro(linha,IKS_ERROR_COND_NOT_BOOL); return tipo_coercao_incompativel(IKS_ERROR_COND_NOT_BOOL);}
					id_nodo++; comp_tree_t* arv = arvore_cria_nodo_pai(IKS_AST_DO_WHILE ,NULL,id_nodo,IKS_TIPO_ESPECIAL,0,(comp_tree_t*) $2,arvC,NULL,NULL);
					arv->tipo = arvore_inferencia_de_tipo(arv->filhos);	
					$$ = (struct comp_tree_t*) arv;
				}; 

entrada		:	TK_PR_INPUT tipo_id
			{
				id_nodo++;
				comp_tree_t* id = (comp_tree_t*) $2;
				comp_tree_t* id_arv = arvore_cria_nodo(IKS_AST_IDENTIFICADOR,id->entrada,NULL,id->id,id->tipo,id->tamanho);
				
				$$ = (struct comp_tree_t*) arvore_cria_nodo_pai(IKS_AST_INPUT ,NULL,id_nodo,id_arv->tipo,0,id_arv,NULL,NULL,NULL);
									
			};

saida		:	TK_PR_OUTPUT lst_saida
			{ 
			  	id_nodo++;
				comp_tree_t* arv = (comp_tree_t*) $2;
				$$ = (struct comp_tree_t*) arvore_cria_nodo_pai(IKS_AST_OUTPUT ,NULL,id_nodo,arv->tipo,0,(comp_tree_t*) $2,NULL,NULL,NULL);
				
			};
lst_saida	:	elem_saida ',' lst_saida 
			{
				id_nodo++;
				$$ = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*)$1,(comp_tree_t*) $3);
				
			}
			| elem_saida {$$ = $1;} ;
elem_saida	:	expr {$$=$1;} ;

retorno		:	TK_PR_RETURN expr
			{
				id_nodo++;
				comp_tree_t* arv = (comp_tree_t*) $2;	
				int tipo_convertido = arv->tipo;
				arv = arvore_coercao(arv,funcao_tipo_retorno);
				if(arv==NULL) {Imprime_Erro(linha,IKS_ERROR_WRONG_PAR_RETURN); return IKS_ERROR_WRONG_PAR_RETURN;}
				$$ = (struct comp_tree_t*) arvore_cria_nodo_pai(IKS_AST_RETURN ,NULL,id_nodo,arv->tipo,0,arv,NULL,NULL,NULL);
									
			}; 

f_cham		:	tipo_id '(' {  	comp_tree_t* id = (comp_tree_t*) $1;
					if(id->entrada->tipo_id==TIPO_ID_VETOR)
						{Imprime_Erro(linha,IKS_ERROR_VECTOR); return IKS_ERROR_VECTOR;}
					else if(id->entrada->tipo_id == TIPO_ID_VARIAVEL)
						{Imprime_Erro(linha,IKS_ERROR_VARIABLE); return IKS_ERROR_VARIABLE;}
				    } passag ')' 
			{
				id_nodo++;
				comp_tree_t* id = (comp_tree_t*) $1;
				comp_tree_t* passagem = (comp_tree_t*) $4;
				
				int passagem_confere = confere_passagem_funcao(tabela_simbolos_escopo->escopo_global,id->entrada->chave,passagem); 
				if(passagem_confere!=0) { Imprime_Erro(linha,passagem_confere); return passagem_confere;}
				comp_tree_t* id_arv = arvore_cria_nodo(IKS_AST_IDENTIFICADOR,id->entrada,NULL,id->id,id->tipo,id->tamanho);
				int tipo_funcao = escopo_retornoFuncao(tabela_simbolos_escopo,id->entrada->chave);
				$$ = (struct comp_tree_t*) arvore_cria_nodo_pai(IKS_AST_CHAMADA_DE_FUNCAO ,NULL,id_nodo,tipo_funcao,0,id_arv,passagem,NULL,NULL);
			};
passag		:	/* empty */ {$$ = NULL;} | lst_p {$$ = $1;} ;
lst_p		:	expr ',' lst_p 
			{
				id_nodo++;
				$$ = (struct comp_tree_t*) arvore_insere_filho((comp_tree_t*) $1,(comp_tree_t*) $3);
				
				
			}
			| expr {$$ = $1;};

expr		:	  '(' expr ')' {$$ = $2;} 
			| expr opArit %prec "expr" expr 
				{ 
					id_nodo++;					
					comp_tree_t* arv = arvore_cria_nodo_pai($2,NULL,id_nodo,IKS_TIPO_ESPECIAL,0,(comp_tree_t*) $1,(comp_tree_t*) $3,NULL,NULL);
					arv->tipo = arvore_inferencia_de_tipo(arv->filhos);	
					$$ = (struct comp_tree_t*)arv;	
				}
			| expr opLog %prec "expr" expr
				{ 
					id_nodo++;					
					comp_tree_t* arv = arvore_cria_nodo_pai($2,NULL,id_nodo,IKS_TIPO_ESPECIAL,0,(comp_tree_t*) $1,(comp_tree_t*) $3,NULL,NULL);
					arv->tipo = IKS_BOOL;	
					$$ = (struct comp_tree_t*) arv;	
				}
			| '-' expr  %prec UMINUS
				{ 
					id_nodo++;
					comp_tree_t* arv = (comp_tree_t*) $2;			
					$$ = (struct comp_tree_t*) arvore_cria_nodo_pai(IKS_AST_ARIM_INVERSAO,NULL,id_nodo,arv->tipo,0,(comp_tree_t*) $2,NULL,NULL,NULL);	
				}
			| '!' expr
				{ 
					id_nodo++;	
					comp_tree_t* arv = (comp_tree_t*) $2;					
					$$ = (struct comp_tree_t*) arvore_cria_nodo_pai(IKS_AST_LOGICO_COMP_NEGACAO,NULL,id_nodo,IKS_BOOL,0,(comp_tree_t*) $2,NULL,NULL,NULL);	
				}	
			| elemento {$$ = $1;};

opArit		: 	  '+'  { $$ =  IKS_AST_ARIM_SOMA; }
			| '-'  { $$ =  IKS_AST_ARIM_SUBTRACAO; }	 	
			| '*'  { $$ =  IKS_AST_ARIM_MULTIPLICACAO; }	
			| '/'  { $$ =  IKS_AST_ARIM_DIVISAO; };

opLog		:	TK_OC_OR { $$ =  IKS_AST_LOGICO_OU; }
			| TK_OC_AND %prec "op" { $$ =  IKS_AST_LOGICO_E; }
			| TK_OC_LE { $$ =  IKS_AST_LOGICO_COMP_LE; }
			| TK_OC_GE { $$ =  IKS_AST_LOGICO_COMP_GE; }
			| TK_OC_EQ { $$ =  IKS_AST_LOGICO_COMP_IGUAL; }
			| TK_OC_NE { $$ =  IKS_AST_LOGICO_COMP_DIF; }
			| '<' { $$ =  IKS_AST_LOGICO_COMP_L; }
			| '>' { $$ =  IKS_AST_LOGICO_COMP_G; };

elemento	:	f_cham {$$ = $1;}
			| tipo_lit {$$ = $1;}
			| acesso_id {$$ = $1;};

acesso_id	:	  tipo_id { 	comp_tree_t* id = (comp_tree_t*) $1;

					if(id->entrada->tipo_id==TIPO_ID_VETOR)
						{Imprime_Erro(linha,IKS_ERROR_VECTOR); return IKS_ERROR_VECTOR;}
					else if(id->entrada->tipo_id == TIPO_ID_FUNCAO)
						{Imprime_Erro(linha,IKS_ERROR_FUNCTION); return IKS_ERROR_FUNCTION;}

					$$ = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_IDENTIFICADOR,id->entrada,NULL,id->id,id->tipo, id->tamanho);} 
			| tipo_id '[' expr ']' 
				{ 
					comp_tree_t* id = (comp_tree_t*) $1;					
					id_nodo++;					
					if(id->entrada->tipo_id==TIPO_ID_VARIAVEL)
						{Imprime_Erro(linha,IKS_ERROR_VARIABLE); return IKS_ERROR_VARIABLE;}
					else if(id->entrada->tipo_id == TIPO_ID_FUNCAO)
						{Imprime_Erro(linha,IKS_ERROR_FUNCTION); return IKS_ERROR_FUNCTION;}
					
					comp_tree_t* arvC = (comp_tree_t*) $3;
					int tipo_convertido = arvC->tipo;
					arvC = arvore_coercao(arvC,IKS_INT);
					if(arvC==NULL) {Imprime_Erro(linha,tipo_coercao_incompativel(tipo_convertido)); return tipo_coercao_incompativel(tipo_convertido);}
					comp_tree_t* id_arv = arvore_cria_nodo(IKS_AST_IDENTIFICADOR,id->entrada,NULL,id->id,id->tipo,id->tamanho);
					
					$$ = (struct comp_tree_t*) arvore_cria_nodo_pai(IKS_AST_VETOR_INDEXADO,NULL,id_nodo,id_arv->tipo,0,id_arv,(comp_tree_t*) $3,NULL,NULL);
				};



tipo		:	  TK_PR_INT 	{ tipo_id = IKS_INT;tamanho_id=tamanho_int; $$ = IKS_INT;}
			| TK_PR_FLOAT 	{ tipo_id = IKS_FLOAT;tamanho_id=tamanho_float; $$ = IKS_FLOAT;} 
			| TK_PR_BOOL 	{ tipo_id = IKS_BOOL;tamanho_id=tamanho_bool; $$=IKS_BOOL;} 
			| TK_PR_CHAR  	{ tipo_id = IKS_CHAR;tamanho_id=tamanho_char; $$=IKS_CHAR;} 
			| TK_PR_STRING 	{ tipo_id = IKS_STRING;tamanho_id=tamanho_char;$$ = IKS_STRING;};

tipo_lit	: 	  lit_int_pos {$$ = $1;}
			| TK_LIT_FLOAT 	{ dicionario = dicionario_AdicionaTipo(yytext,IKS_FLOAT,tamanho_float,dicionario);
					  comp_dict_t* entrada = dicionario_LocalizarEntrada(yytext, dicionario);
					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  $$ = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo,IKS_FLOAT,tamanho_float);					  
					} 
			| TK_LIT_CHAR 	{ dicionario = dicionario_AdicionaTipo(sem_primeiro,IKS_CHAR,tamanho_char,dicionario);
				          comp_dict_t* entrada = dicionario_LocalizarEntrada(sem_primeiro, dicionario);
					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  $$ = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo,IKS_CHAR,tamanho_char);
					} 
			| tipo_string { $$ = $1;}
			| TK_LIT_TRUE 	{ dicionario = dicionario_AdicionaTipo(yytext,IKS_BOOL,tamanho_bool,dicionario);
					  comp_dict_t* entrada = dicionario_LocalizarEntrada(yytext, dicionario);
					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  $$ = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo,IKS_BOOL,tamanho_bool);
					} 
			| TK_LIT_FALSE 	{ dicionario = dicionario_AdicionaTipo(yytext,IKS_BOOL,tamanho_bool,dicionario);
					  comp_dict_t* entrada = dicionario_LocalizarEntrada(yytext, dicionario);
					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  $$ = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo,IKS_BOOL,tamanho_bool);
					};
lit_int_pos	:	TK_LIT_INT 	{ dicionario = dicionario_AdicionaTipo(yytext,IKS_INT,tamanho_int,dicionario);
					  comp_dict_t* entrada = dicionario_LocalizarEntrada(yytext, dicionario);
					  int ast_id =  IKS_AST_LITERAL;
					  valor_vetor_id = atoi(yytext);
					  if(nodo_nao_valido==0)
					  {
						id_nodo++;
				   	  	$$ = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo,IKS_INT,tamanho_int);
					  }
					  else { $$ = (struct comp_tree_t*) arvore_cria_nodo_inutil(entrada,id_nodo,IKS_INT,tamanho_int);}
					
					};

tipo_id		:	TK_IDENTIFICADOR { 
						ultimo_id = (char*)malloc(strlen(yytext)); strcpy(ultimo_id,yytext);	   						
						if(declaracao_id==1)
						{ 
							comp_dict_t* entrada = dicionario_Criar_Entrada(dicionario_LocalizarEntrada(yytext, dicionario));
							id_nodo++;
							$$ = (struct comp_tree_t*) arvore_cria_nodo_inutil(entrada,id_nodo,tipo_id,tamanho_id);
						}
						else
						{
							comp_dict_t* entrada = dicionario_Criar_Entrada(escopo_localizarEm_LocalAtual_Global(tabela_simbolos_escopo,yytext));
							if(entrada==NULL) { Imprime_Erro(linha,IKS_ERROR_UNDECLARED); return IKS_ERROR_UNDECLARED;}
									   
							id_nodo++;
							$$ = (struct comp_tree_t*) arvore_cria_nodo_inutil(entrada,id_nodo,entrada->tipo,entrada->tamanho);
						}
				   	 } ;
tipo_string	:	TK_LIT_STRING {   					  
					  dicionario = dicionario_AdicionaTipo(sem_primeiro,IKS_STRING,tam_string*tamanho_char,dicionario);
						
					  comp_dict_t* entrada = dicionario_LocalizarEntrada(sem_primeiro, dicionario);

					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  $$ = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo,IKS_STRING,tam_string*tamanho_char);

					} ;
					 

//
%%


